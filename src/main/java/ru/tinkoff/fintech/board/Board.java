package ru.tinkoff.fintech.board;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Board - один из возможных видов форм.
 * При создании для доски (board) устанавливается id связанной формы (form).
 */

@Data
@Entity
@Table(name = "board")
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class Board {
    @Id
    @Column(name = "board_id")
    private Long id;

    @Column(name = "board_name")
    @NotBlank(message = "Board name cannot be blank")
    private String name;

    @Column(name = "board_description")
    @NotNull(message = "Board description cannot be null")
    private String description;

    @Column(name = "created_date", updatable = false)
    @CreatedDate
    private LocalDateTime createdDate;

    @Column(name = "updated_date")
    @LastModifiedDate
    private LocalDateTime updatedDate;
}
