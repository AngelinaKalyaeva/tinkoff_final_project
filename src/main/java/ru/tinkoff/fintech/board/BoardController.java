package ru.tinkoff.fintech.board;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.fintech.board.dto.BoardDto;
import ru.tinkoff.fintech.status.dto.StatusBoardDto;
import ru.tinkoff.fintech.task.Task;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/board")
@RequiredArgsConstructor
public class BoardController {
    private final BoardService boardService;

    @PostMapping
    public Board addBoardToSpace(@RequestBody @Valid BoardDto boardDto) {
        return boardService.createBoard(boardDto.getBoard(), boardDto.getForm());
    }

    @GetMapping("/{id}")
    public BoardDto getBoard(@PathVariable("id") @NotNull Long id) {
        return boardService.getBoardById(id);
    }

    @PutMapping
    public Board updateBoard(@RequestBody @Valid Board board) {
        return boardService.updateBoard(board);
    }

    @DeleteMapping("/{id}")
    public void deleteBoard(@PathVariable("id") @NotNull Long id) {
        boardService.deleteBoard(id);
    }

    @PostMapping("/{boardId}/add/status/{statusName}")
    public void addStatusOnBoard(
            @PathVariable("boardId") @NotNull Long boardId,
            @PathVariable("statusName") @NotBlank String statusName
    ) {
        boardService.addStatusOnBoard(boardId, statusName);
    }

    @PutMapping("/status")
    public void updateStatusOnBoard(@RequestBody @Valid StatusBoardDto statusBoardDto) {
        boardService.updateStatusOnBoard(statusBoardDto.getStatusBoard(), statusBoardDto.getNewStatusName());
    }

    @PutMapping("/{boardId}/statuses/moving")
    public void movingStatusesOnBoard(
            @PathVariable("boardId") @NotNull Long boardId,
            @RequestBody @NotNull List<Long> statuses
    ) {
        boardService.movingStatusesOnBoard(boardId, statuses);
    }

    @DeleteMapping("/{boardId}/delete/status/{statusId}")
    public void deleteStatusFromBoard(
            @PathVariable("boardId") @NotNull Long boardId,
            @PathVariable("statusId") long statusId
    ) {
        boardService.deleteStatusFromBoard(boardId, statusId);
    }

    @PostMapping("/add/task")
    public Task addTaskOnBoard(@RequestBody @Valid Task task) {
        return boardService.addTaskOnBoard(task);
    }

    @PutMapping("/{sourceBoardId}/migrate/{targetBoardId}")
    public void migrate(
            @PathVariable("sourceBoardId") @NotNull Long sourceBoardId,
            @PathVariable("targetBoardId") @NotNull Long targetBoardId
    ) {
        boardService.migrateBoard(sourceBoardId, targetBoardId);
    }
}
