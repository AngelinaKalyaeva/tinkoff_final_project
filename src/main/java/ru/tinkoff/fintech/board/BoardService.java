package ru.tinkoff.fintech.board;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.fintech.board.dto.BoardDto;
import ru.tinkoff.fintech.form.Form;
import ru.tinkoff.fintech.form.FormService;
import ru.tinkoff.fintech.form.FormType;
import ru.tinkoff.fintech.status.Status;
import ru.tinkoff.fintech.status.StatusBoard;
import ru.tinkoff.fintech.status.StatusBoardService;
import ru.tinkoff.fintech.status.StatusService;
import ru.tinkoff.fintech.task.Task;
import ru.tinkoff.fintech.task.TaskService;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BoardService {
    private final BoardRepository boardRepository;
    private final FormService formService;
    private final StatusService statusService;
    private final StatusBoardService statusBoardService;
    private final TaskService taskService;

    /**
     * Создание доски.
     * Для создания доски необходимо передать:
     * - содержимое доски (board);
     * - область рабочего пространства, где доска должна разместиться (form).
     */
    @Transactional
    public Board createBoard(Board board, Form form) {
        form.setTypeId(FormType.BOARD.formId);
        Form createdForm = formService.createForm(form);
        board.setId(createdForm.getId());
        board.setUpdatedDate(null);
        return boardRepository.save(board);
    }

    @Transactional
    public BoardDto getBoardById(Long id) {
        Board board = boardRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Board not found by id: " + id));

        Form form = formService.getForm(id);

        List<Long> statusBoardsId = statusBoardService.getAllStatusBoardByBoardId(id)
                .stream()
                .map(StatusBoard::getStatusId)
                .toList();

        List<Status> statuses = statusService.getAllById(statusBoardsId);

        Map<Long, List<Task>> taskToStatusMap = taskService.getAllTasksByBoardId(id)
                .stream()
                .collect(Collectors.groupingBy(Task::getStatusId));

        return new BoardDto(board, form, statuses, taskToStatusMap);
    }

    @Transactional
    public Board updateBoard(Board board) {
        Long boardId = board.getId();
        if (boardId == null) {
            throw new IllegalArgumentException("Board with null id cannot be updated");
        }
        Board boardFromDb = boardRepository.findById(boardId)
                .orElseThrow(() -> new NoSuchElementException("Task not found by id: " + boardId));
        board.setCreatedDate(boardFromDb.getCreatedDate());
        return boardRepository.save(board);
    }

    @Transactional
    public void deleteBoard(Long id) {
        taskService.batchDeleteTasksByBoardId(id);
        statusBoardService.deleteAllStatusesFromBoard(id);
        boardRepository.deleteById(id);
        formService.deleteForm(id);
    }

    @Transactional
    public void addStatusOnBoard(Long boardId, String statusName) {
        if (isBoardNotExist(boardId)) {
            throw new IllegalArgumentException("Incorrect board id: " + boardId);
        }
        Long statusId = statusService.createOrGetStatus(statusName).getId();
        statusBoardService.createStatusBoard(new StatusBoard(boardId, statusId));
        boardRepository.findById(boardId)
                .map(el -> el.setUpdatedDate(LocalDateTime.now()))
                .ifPresent(boardRepository::save);
    }

    @Transactional
    public void updateStatusOnBoard(StatusBoard statusBoard, String newStatusName) {
        Status status = statusService.createOrGetStatus(newStatusName);
        List<Task> allTasksByBoardId = taskService.getAllTasksByBoardIdAndStatusId(
                        statusBoard.getBoardId(),
                        statusBoard.getStatusId()
                ).stream()
                .map(el -> el.setStatusId(status.getId()))
                .toList();
        taskService.batchUpdateTasks(allTasksByBoardId);

        deleteStatusFromBoard(statusBoard.getBoardId(), statusBoard.getStatusId());

        StatusBoard newStatusBoard = new StatusBoard(
                statusBoard.getBoardId(),
                status.getId(),
                statusBoard.getStatusNumber()
        );
        statusBoardService.createStatusBoard(newStatusBoard);
        boardRepository.findById(statusBoard.getBoardId())
                .map(el -> el.setUpdatedDate(LocalDateTime.now()))
                .ifPresent(boardRepository::save);
    }

    @Transactional
    public void deleteStatusFromBoard(Long boardId, long statusId) {
        taskService.batchDeleteTasksByStatusId(statusId);
        Long statusBoardId = statusBoardService.getStatusBoardByBoardIdAndStatusId(boardId, statusId).getId();
        statusBoardService.deleteStatusBoard(statusBoardId);
        boardRepository.findById(boardId)
                .map(el -> el.setUpdatedDate(LocalDateTime.now()))
                .ifPresent(boardRepository::save);
    }

    @Transactional
    public void movingStatusesOnBoard(Long boarId, List<Long> statusesId) {
        Map<Long, StatusBoard> statusesOnBoard = statusBoardService.getAllStatusBoardByBoardId(boarId)
                .stream()
                .collect(Collectors.toMap(StatusBoard::getStatusId, Function.identity()));
        if (statusesOnBoard.keySet().isEmpty()) {
            throw new IllegalArgumentException("Statuses not found on board with board id: " + boarId);
        }
        List<StatusBoard> statusBoardsToUpdate = new ArrayList<>();
        for (int newStatusNumber = 0; newStatusNumber < statusesId.size(); newStatusNumber++) {
            Long statusId = statusesId.get(newStatusNumber);
            StatusBoard statusBoard = statusesOnBoard.get(statusId);
            if (statusBoard == null) {
                throw new IllegalArgumentException(
                        "Status with status id: " + statusId + "not found on board with board id: " + boarId
                );
            }
            statusBoard.setStatusNumber(newStatusNumber);
            statusBoardsToUpdate.add(statusBoard);
        }
        statusBoardService.batchUpdateStatusBoards(statusBoardsToUpdate);
        boardRepository.findById(boarId)
                .map(el -> el.setUpdatedDate(LocalDateTime.now()))
                .ifPresent(boardRepository::save);
    }

    @Transactional
    public void migrateBoard(Long sourceBoardId, Long targetBoardId) {
        if (isBoardNotExist(sourceBoardId) || isBoardNotExist(targetBoardId)) {
            throw new IllegalArgumentException("Board not exist");
        }
        List<StatusBoard> sourceStatusBoards = statusBoardService.getAllStatusBoardByBoardId(sourceBoardId);
        if (sourceStatusBoards.isEmpty()) {
            throw new IllegalArgumentException("Incorrect source board id: " + sourceBoardId);
        }

        List<StatusBoard> targetStatusBoards = statusBoardService.getAllStatusBoardByBoardId(targetBoardId);
        List<Long> targetStatusesId = targetStatusBoards.stream().map(StatusBoard::getStatusId).toList();
        List<StatusBoard> statusesToMigrate = sourceStatusBoards.stream()
                .filter(el -> !targetStatusesId.contains(el.getStatusId()))
                .map(el -> new StatusBoard(targetBoardId, el.getStatusId()))
                .toList();

        statusBoardService.batchCreateStatusBoards(statusesToMigrate);
        List<Task> tasksFromSourceBoard = taskService.getAllTasksByBoardId(sourceBoardId);
        List<Task> tasksToMigrate = new ArrayList<>();
        for (Task taskToMigrate : tasksFromSourceBoard) {
            Task copyTask = new Task(taskToMigrate);
            copyTask.setBoardId(targetBoardId);
            tasksToMigrate.add(copyTask);
        }
        taskService.batchCreateTasks(tasksToMigrate);

        boardRepository.findById(targetBoardId)
                .map(el -> el.setUpdatedDate(LocalDateTime.now()))
                .ifPresent(boardRepository::save);
    }

    @Transactional
    public Task addTaskOnBoard(Task task) {
        Long boardId = task.getBoardId();
        if (isBoardNotExist(boardId)) {
            throw new IllegalArgumentException("Wrong board id. Task cannot be added");
        }
        long statusId = task.getStatusId();
        List<StatusBoard> allStatusBoardByBoardId = statusBoardService.getAllStatusBoardByBoardId(boardId);
        boolean isStatusOnBoard = allStatusBoardByBoardId.stream()
                .anyMatch(el -> el.getStatusId() == statusId);
        if (!isStatusOnBoard) {
            throw new IllegalArgumentException(
                    "Status with status id: " + statusId + "not found on board with id: " + boardId
            );
        }
        boardRepository.findById(boardId)
                .map(el -> el.setUpdatedDate(LocalDateTime.now()))
                .ifPresent(boardRepository::save);
        return taskService.createTask(task);
    }

    private boolean isBoardNotExist(Long boardId) {
        return boardRepository.findById(boardId).isEmpty();
    }
}
