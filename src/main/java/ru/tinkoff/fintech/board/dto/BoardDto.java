package ru.tinkoff.fintech.board.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.tinkoff.fintech.form.Form;
import ru.tinkoff.fintech.board.Board;
import ru.tinkoff.fintech.status.Status;
import ru.tinkoff.fintech.task.Task;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
public class BoardDto {
    @Valid
    private Board board;

    @Valid
    private Form form;

    private List<Status> statuses;

    private Map<Long, List<Task>> tasksByStatusId;
}
