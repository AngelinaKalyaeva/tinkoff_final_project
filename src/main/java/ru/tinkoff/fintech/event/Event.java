package ru.tinkoff.fintech.event;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Event - часть roadmap.
 * При создании события (event) оно закрепляется за конкретной roadmap.
 */

@Data
@Entity
@Table(name = "event")
public class Event {
    @Id
    @Column(name = "event_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "roadmap_id")
    @NotNull
    private Long roadmapId;

    @Column(name = "event_name")
    @NotBlank(message = "Event name cannot be blank")
    private String name;

    @Column(name = "short_description")
    @NotNull
    private String description;

    @Column(name = "planned_start_date")
    private LocalDateTime plannedStartDate;

    @Column(name = "planned_end_date")
    private LocalDateTime plannedEndDate;

    @Column(name = "actual_start_date")
    private LocalDateTime actualStartDate;

    @Column(name = "actual_end_date")
    private LocalDateTime actualEndDate;

    @Column(name = "assigned_user")
    private String assignedUsername;
}
