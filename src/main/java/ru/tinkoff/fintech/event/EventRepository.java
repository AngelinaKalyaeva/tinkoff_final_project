package ru.tinkoff.fintech.event;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRepository extends CrudRepository<Event, Long> {
    List<Event> findAllByRoadmapId(Long roadmapId);

    void deleteAllByRoadmapId(Long roadmapId);
}
