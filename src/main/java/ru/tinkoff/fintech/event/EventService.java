package ru.tinkoff.fintech.event;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EventService {
    private final EventRepository eventRepository;

    @Transactional
    public Event createEvent(Event event) {
        if (event.getId() != null) {
            throw new IllegalArgumentException("Event with not null id cannot be created");
        }
        checkEventDates(event);
        if (isValidActualDates(event)) {
            throw new IllegalArgumentException("Actual dates cannot be not null");
        }
        return eventRepository.save(event);
    }

    public List<Event> getAllByRoadmapId(Long roadmapId) {
        return eventRepository.findAllByRoadmapId(roadmapId);
    }

    @Transactional
    public void updateEvent(Event event) {
        if (event.getId() == null) {
            throw new IllegalArgumentException("Event with null id cannot be updated");
        }
        checkEventDates(event);
        LocalDateTime actualStartDate = event.getActualStartDate();
        LocalDateTime actualEndDate = event.getActualEndDate();
        if (isValidUpdateActualDates(actualStartDate, actualEndDate)) {
            throw new IllegalArgumentException("Wrong actual event dates");
        }
        eventRepository.save(event);
    }

    public void deleteEvent(Long id) {
        eventRepository.deleteById(id);
    }

    public void deleteAllByRoadmapId(Long id) {
        eventRepository.deleteAllByRoadmapId(id);
    }

    private void checkEventDates(Event event) {
        LocalDateTime deadlineDate = event.getPlannedEndDate();
        LocalDateTime startDate = event.getPlannedStartDate();
        if (startDate == null || deadlineDate == null) {
            throw new IllegalArgumentException("Planing date cannot be null");
        }
        if (deadlineDate.isBefore(startDate)) {
            throw new IllegalArgumentException("Deadline date cannot be before start date");
        }
    }

    private boolean isValidActualDates(Event event) {
        return event.getActualStartDate() != null || event.getActualEndDate() != null;
    }

    private boolean isValidUpdateActualDates(LocalDateTime actualStartDate, LocalDateTime actualEndDate) {
        return (actualStartDate == null && actualEndDate != null) ||
                (actualStartDate != null && actualStartDate.isAfter(actualEndDate));
    }
}
