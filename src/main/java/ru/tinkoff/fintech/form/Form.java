package ru.tinkoff.fintech.form;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Form - область пространства, которая характеризуется координатами и размерами.
 * В дальнейшем при создании конкретного элемента (board, note, roadmap) он ассоциируется с формой.
 */

@Data
@Entity
@Table(name = "form")
public class Form {
    @Id
    @Column(name = "form_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "space_id")
    @NotNull
    private Long spaceId;

    @Column(name = "type_id")
    private int typeId;

    @Column(name = "x_coordinate")
    private double xCoordinate;

    @Column(name = "y_coordinate")
    private  double yCoordinate;

    @Column(name = "height")
    private double height;

    @Column(name = "length")
    private double length;
}
