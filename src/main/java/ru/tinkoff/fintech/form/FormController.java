package ru.tinkoff.fintech.form;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.fintech.form.dto.FormsDto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/form")
@RequiredArgsConstructor
public class FormController {
    private final FormService formService;

    @GetMapping("/{spaceId}")
    public FormsDto getAllFormsForSpace(@PathVariable("spaceId") @NotNull Long spaceId) {
        return formService.getAllFormsDtoBySpaceId(spaceId);
    }

    @PutMapping
    public Form updateForm(@RequestBody @Valid Form form) {
        return formService.updateForm(form);
    }
}
