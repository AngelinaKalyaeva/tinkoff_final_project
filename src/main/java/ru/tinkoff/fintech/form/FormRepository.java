package ru.tinkoff.fintech.form;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FormRepository extends CrudRepository<Form, Long> {
    List<Form> findAllFormBySpaceId (Long spaceId);
}
