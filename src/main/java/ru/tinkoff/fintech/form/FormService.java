package ru.tinkoff.fintech.form;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.fintech.space.Space;
import ru.tinkoff.fintech.space.SpaceService;
import ru.tinkoff.fintech.form.dto.FormsDto;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FormService {
    private final FormRepository formRepository;
    private final SpaceService spaceService;

    @Transactional
    public Form createForm(Form form) {
        if (form.getId() != null) {
            throw new IllegalArgumentException("Form with not null id cannot be created");
        }
        Long spaceId = form.getSpaceId();
        if (spaceService.isSpaceNotExist(spaceId)) {
            throw new IllegalArgumentException("Wrong space id. Form cannot be created");
        }
        return formRepository.save(form);
    }

    public Form getForm(Long id) {
        return formRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Form not found by id " + id));
    }

    @Transactional
    public FormsDto getAllFormsDtoBySpaceId(Long spaceId) {
        Space space = spaceService.getSpaceById(spaceId);
        List<Form> forms = getAllFormsBySpaceId(spaceId);
        return new FormsDto(space,forms);
    }

    public List<Form> getAllFormsBySpaceId(Long spaceId) {
        return formRepository.findAllFormBySpaceId(spaceId);
    }

    public Form updateForm(Form form) {
        if (form.getId() == null) {
            throw new IllegalArgumentException("Form with null id cannot be updated");
        }
        return formRepository.save(form);
    }

    public void deleteForm(Long id) {
        formRepository.deleteById(id);
    }
}
