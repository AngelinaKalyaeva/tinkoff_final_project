package ru.tinkoff.fintech.form;

public enum FormType {
    BOARD(1),
    NOTE(2),
    ROADMAP(3);

    public final int formId;

    FormType(int formId) {
        this.formId = formId;
    }
}
