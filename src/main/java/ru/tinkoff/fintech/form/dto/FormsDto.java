package ru.tinkoff.fintech.form.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.tinkoff.fintech.form.Form;
import ru.tinkoff.fintech.space.Space;

import java.util.List;

@Data
@AllArgsConstructor
public class FormsDto {
    private Space space;
    private List<Form> forms;
}
