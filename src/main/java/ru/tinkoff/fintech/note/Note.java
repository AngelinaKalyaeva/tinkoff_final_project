package ru.tinkoff.fintech.note;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/**
 * Note - один из возможных видов форм.
 * При создании для заметки (note) устанавливается id связанной формы (form).
 */

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
@Accessors(chain = true)
@Table(name = "note")
public class Note {
    @Id
    @Column(name = "note_id")
    private Long id;

    @Column(name = "note_name")
    @NotBlank(message = "Note name cannot be blank")
    private String name;

    @Column(name = "text")
    @NotNull
    private String content;

    @Column(name = "created_date", updatable = false)
    @CreatedDate
    private LocalDateTime createdDate;

    @Column(name = "updated_date")
    @LastModifiedDate
    private LocalDateTime updatedDate;

    @Column(name = "linked_note_id")
    private Long linkedNoteId;
}
