package ru.tinkoff.fintech.note;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.fintech.note.dto.NoteDto;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/note")
@RequiredArgsConstructor
public class NoteController {
    private final NoteService noteService;

    @GetMapping("/{id}")
    public NoteDto getNote(@PathVariable("id") @NotNull Long id) {
        return noteService.getNoteById(id);
    }

    @PostMapping
    public Note addNoteToSpace(@RequestBody @Valid NoteDto noteDto) {
        return noteService.createNote(noteDto.getNote(), noteDto.getForm());
    }

    @PutMapping
    public Note updateNote(@RequestBody @Valid Note note) {
        return noteService.updateNote(note);
    }

    @DeleteMapping("/{id}")
    public void deleteNote(@PathVariable("id") @NotNull Long id) {
        noteService.deleteNote(id);
    }

    @PostMapping("/tag/add")
    public void addTagToNote(@RequestParam @NotNull Long noteId, @RequestParam @NotBlank String tagName) {
        noteService.addTagToNote(noteId, tagName);
    }

    @GetMapping("/tag/{tagName}")
    public List<Long> getNotesByTagName(@PathVariable("tagName") @NotBlank String tagName) {
        return noteService.getAllNotesIdByTagName(tagName);
    }

    @DeleteMapping("/{noteId}/delete/tag/{tagId}")
    public void deleteTagFromNote(@PathVariable("noteId") @NotNull Long noteId, @PathVariable("tagId") long tagId) {
        noteService.deleteTagFromNote(noteId, tagId);
    }
}
