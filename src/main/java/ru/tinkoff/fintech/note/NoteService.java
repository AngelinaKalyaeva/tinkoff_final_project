package ru.tinkoff.fintech.note;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.fintech.form.Form;
import ru.tinkoff.fintech.form.FormService;
import ru.tinkoff.fintech.form.FormType;
import ru.tinkoff.fintech.note.dto.NoteDto;
import ru.tinkoff.fintech.tag.Tag;
import ru.tinkoff.fintech.tag.TagService;
import ru.tinkoff.fintech.tag.note.TagNote;
import ru.tinkoff.fintech.tag.note.TagNoteService;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
public class NoteService {
    private final NoteRepository noteRepository;
    private final FormService formService;
    private final TagService tagService;
    private final TagNoteService tagNoteService;

    /**
     *Создание заметки.
     * Для создания необходимо передать:
     * - содержимое заметки (note);
     * - область рабочего пространства, где заметка должна разместиться (form).
     */
    @Transactional
    public Note createNote(Note note, Form form) {
        form.setTypeId(FormType.NOTE.formId);
        Form createdForm = formService.createForm(form);
        note.setId(createdForm.getId());
        note.setUpdatedDate(null);
        return noteRepository.save(note);
    }

    @Transactional
    public NoteDto getNoteById(Long id) {
        Note note = noteRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Note not found by id: " + id));
        Form form = formService.getForm(id);
        return new NoteDto(note, form);
    }

    @Transactional
    public Note updateNote(Note note) {
        Long noteId = note.getId();
        if (noteId == null) {
            throw new IllegalArgumentException("Note with null id cannot updated");
        }
        if (isValidNoteId(note)) {
            throw new IllegalArgumentException("Wrong linked note id");
        }
        Note noteFromDb = noteRepository.findById(noteId)
                .orElseThrow(() -> new NoSuchElementException("Task not found by id: " + noteId));
        note.setCreatedDate(noteFromDb.getCreatedDate());
        return noteRepository.save(note);
    }

    @Transactional
    public void deleteNote(Long id) {
        noteRepository.deleteById(id);
        formService.deleteForm(id);
    }

    @Transactional
    public void addTagToNote(Long noteId, String tagName) {
        if (isNoteNotExist(noteId)) {
            throw new IllegalArgumentException("Note with note id: " + noteId + "not found");
        }

        Long tagId = tagService.createTag(tagName).getId();
        TagNote tagNote = new TagNote(noteId, tagId);
        tagNoteService.createTagNote(tagNote);
    }

    @Transactional
    public List<Long> getAllNotesIdByTagName(String tagName) {
        Tag tag = tagService.getTag(tagName);
        return tagNoteService.getAllNotesIdByTagId(tag.getId());
    }

    @Transactional
    public void deleteTagFromNote(Long noteId, long tagId) {
        Long tagNoteId = tagNoteService.getTagNotByNoteIdAndTagId(noteId, tagId).getId();
        tagNoteService.deleteTagNote(tagNoteId);
    }

    private boolean isNoteNotExist(Long id) {
        return noteRepository.findById(id).isEmpty();
    }

    private boolean isValidNoteId(Note note) {
        return note.getLinkedNoteId() != null && isNoteNotExist(note.getLinkedNoteId());
    }
}
