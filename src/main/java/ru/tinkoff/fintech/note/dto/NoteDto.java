package ru.tinkoff.fintech.note.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.tinkoff.fintech.form.Form;
import ru.tinkoff.fintech.note.Note;

import javax.validation.Valid;

@Data
@AllArgsConstructor
public class NoteDto {
    @Valid
    private Note note;

    @Valid
    private Form form;
}
