package ru.tinkoff.fintech.roadmap;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/**
 * Roadmap - один из возможных видов форм.
 * При создании для roadmap устанавливается id связанной формы (form).
 */

@Data
@Entity
@Table(name = "roadmap")
public class Roadmap {
    @Id
    @Column(name = "roadmap_id")
    private Long id;

    @Column(name = "roadmap_name")
    @NotBlank(message = "Roadmap name cannot be blank")
    private String name;

    @Column(name = "start_date")
    private LocalDateTime startDate;

    @Column(name = "end_date")
    private LocalDateTime endDate;
}
