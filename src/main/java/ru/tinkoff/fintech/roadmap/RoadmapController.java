package ru.tinkoff.fintech.roadmap;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.fintech.event.Event;
import ru.tinkoff.fintech.roadmap.dto.RoadmapDto;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/roadmap")
@RequiredArgsConstructor
public class RoadmapController {
    private final RoadmapService roadmapService;

    @PostMapping
    public Roadmap addRoadmapToSpace(@RequestBody @Valid RoadmapDto roadmapDto) {
        return roadmapService.createRoadmap(roadmapDto.getRoadmap(), roadmapDto.getForm());
    }

    @GetMapping("/{id}")
    public RoadmapDto getRoadmap(@PathVariable("id") @NotNull Long id) {
        return roadmapService.getRoadmap(id);
    }

    @PutMapping
    public void updateRoadmap(@RequestBody @Valid Roadmap roadmap) {
        roadmapService.updateRoadmap(roadmap);
    }

    @DeleteMapping("/{roadmapId}")
    public void deleteRoadmap(@PathVariable("roadmapId") @NotNull Long roadmapId) {
        roadmapService.deleteRoadmap(roadmapId);
    }

    @PostMapping("/add/event")
    public Event addEventToRoadmap(@RequestBody @Valid Event event) {
        return roadmapService.addEventToRoadmap(event);
    }

    @GetMapping("/{roadmapId}/user/{username}/statistic/")
    public double getStatisticByUsername(
            @PathVariable("roadmapId") @NotNull Long roadmapId,
            @PathVariable("username") @NotBlank String username
    ) {
        return roadmapService.getEventSuccessRateByUser(roadmapId, username);
    }
}
