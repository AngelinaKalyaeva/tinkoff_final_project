package ru.tinkoff.fintech.roadmap;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoadmapRepository extends CrudRepository<Roadmap, Long> {

}
