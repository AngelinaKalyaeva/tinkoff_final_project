package ru.tinkoff.fintech.roadmap;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.fintech.event.Event;
import ru.tinkoff.fintech.event.EventService;
import ru.tinkoff.fintech.form.Form;
import ru.tinkoff.fintech.form.FormService;
import ru.tinkoff.fintech.form.FormType;
import ru.tinkoff.fintech.roadmap.dto.RoadmapDto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
public class RoadmapService {
    private final RoadmapRepository roadMapRepository;
    private final FormService formService;
    private final EventService eventService;

    /**
     *Создание roadmap.
     * Для создания необходимо передать:
     * - содержимое (roadmap);
     * - область рабочего пространства, где roadmap должна разместиться (form).
     */
    @Transactional
    public Roadmap createRoadmap(Roadmap roadmap, Form form) {
        form.setTypeId(FormType.ROADMAP.formId);
        formService.createForm(form);
        roadmap.setId(form.getId());
        return roadMapRepository.save(roadmap);
    }

    @Transactional
    public RoadmapDto getRoadmap(Long id) {
        Roadmap roadmap = roadMapRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Roadmap not found by id: " + id));
        Form form = formService.getForm(id);
        List<Event> events = eventService.getAllByRoadmapId(id);
        return new RoadmapDto(roadmap, form, events);
    }

    public void updateRoadmap(Roadmap roadmap) {
        if (roadmap.getId() == null) {
            throw new IllegalArgumentException("Roadmap with null id cannot be updated");
        }
        roadMapRepository.save(roadmap);
    }

    @Transactional
    public void deleteRoadmap(Long id) {
        eventService.deleteAllByRoadmapId(id);
        roadMapRepository.deleteById(id);
        formService.deleteForm(id);
    }

    @Transactional
    public Event addEventToRoadmap(Event event) {
        Long roadmapId = event.getRoadmapId();
        Roadmap roadmap = roadMapRepository.findById(roadmapId)
                .orElseThrow(() -> new NoSuchElementException("Roadmap not found by id:" + roadmapId));
        if (isValidPlannedDates(event, roadmap)) {
            throw new IllegalArgumentException("Event must not go beyond the roadmap");
        }
        return eventService.createEvent(event);
    }

    public double getEventSuccessRateByUser(Long roadmapId, String username) {
        List<Event> eventsByUsername = eventService.getAllByRoadmapId(roadmapId).stream()
                .filter(el -> el.getAssignedUsername().equals(username))
                .toList();
        double successEventCount = eventsByUsername.stream().filter(this::isOnScheduleExecutedEvent).count();
        return successEventCount / eventsByUsername.size();
    }

    private boolean isOnScheduleExecutedEvent(Event event) {
        LocalDateTime actualEndDate = event.getActualEndDate();
        return actualEndDate != null && actualEndDate.isBefore(event.getPlannedEndDate());
    }

    private boolean isValidPlannedDates(Event event, Roadmap roadmap) {
        return event.getPlannedStartDate().isBefore(roadmap.getStartDate()) ||
                event.getPlannedEndDate().isAfter(roadmap.getEndDate());
    }
}
