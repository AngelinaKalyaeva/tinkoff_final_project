package ru.tinkoff.fintech.roadmap.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.tinkoff.fintech.event.Event;
import ru.tinkoff.fintech.form.Form;
import ru.tinkoff.fintech.roadmap.Roadmap;

import javax.validation.Valid;
import java.util.List;

@Data
@AllArgsConstructor
public class RoadmapDto {
    @Valid
    private Roadmap roadmap;

    @Valid
    private Form form;

    private List<Event> events;
}
