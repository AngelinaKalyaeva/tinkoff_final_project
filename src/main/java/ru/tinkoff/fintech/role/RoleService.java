package ru.tinkoff.fintech.role;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
public class RoleService {
    private final RoleRepository roleRepository;

    public Role getRoleById(Long id) {
        return roleRepository.findById(id).orElseThrow(() -> new NoSuchElementException("Role not found by id: " + id));
    }

    public Role getRoleByName(String name) {
        return roleRepository.findRoleByName(name)
                .orElseThrow(() -> new NoSuchElementException("Role not found by name: " + name));
    }
}
