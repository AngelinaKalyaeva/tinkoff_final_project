package ru.tinkoff.fintech.security;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.role.RoleService;
import ru.tinkoff.fintech.user.User;
import ru.tinkoff.fintech.user.UserService;

import static org.springframework.security.core.userdetails.User.UserBuilder;
import static org.springframework.security.core.userdetails.User.withUsername;

@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {
    private final UserService userService;
    private final RoleService roleService;

    @Override
    public UserDetails loadUserByUsername(String username) {
        UserBuilder userBuilder = withUsername(username);
        User user = userService.getUserById(username);
        userBuilder.password(user.getPassword());
        userBuilder.roles(roleService.getRoleById(user.getRoleId()).getName());
        return userBuilder.build();
    }
}
