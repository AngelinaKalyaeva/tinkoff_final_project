package ru.tinkoff.fintech.security;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    public static final String ADMIN_ROLE_NAME = "admin";
    public static final String PARTICIPANT_ROLE_NAME = "participant";

    private final CustomUserDetailsService userDetailsService;
    private final PasswordEncoder passwordEncoder;

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/swagger**").hasAnyRole(ADMIN_ROLE_NAME, PARTICIPANT_ROLE_NAME)
                //user controller
                .antMatchers(HttpMethod.POST, "/user/registration").hasRole(ADMIN_ROLE_NAME)
                .and()
                .authorizeRequests()
                //board controller
                .antMatchers(HttpMethod.POST, "/board").hasRole(ADMIN_ROLE_NAME)
                .antMatchers(HttpMethod.GET, "/board/**").hasAnyRole(ADMIN_ROLE_NAME, PARTICIPANT_ROLE_NAME)
                .antMatchers(HttpMethod.PUT, "/board").hasRole(ADMIN_ROLE_NAME)
                .antMatchers(HttpMethod.DELETE, "/board/**").hasRole(ADMIN_ROLE_NAME)
                .antMatchers(HttpMethod.POST, "/board/**/add/status/**").hasRole(ADMIN_ROLE_NAME)
                .antMatchers(HttpMethod.PUT, "/board/status").hasRole(ADMIN_ROLE_NAME)
                .antMatchers(HttpMethod.PUT, "/board/**/statuses/moving").hasRole(ADMIN_ROLE_NAME)
                .antMatchers(HttpMethod.DELETE, "/board/**/delete/status/**").hasRole(ADMIN_ROLE_NAME)
                .antMatchers(HttpMethod.POST, "/board/add/task").hasRole(ADMIN_ROLE_NAME)
                .antMatchers(HttpMethod.PUT, "/board/**/migrate/**").hasRole(ADMIN_ROLE_NAME)
                //event controller
                .antMatchers(HttpMethod.PUT, "/event").hasRole(ADMIN_ROLE_NAME)
                .antMatchers(HttpMethod.DELETE, "/event/**").hasRole(ADMIN_ROLE_NAME)
                //form controller
                .antMatchers(HttpMethod.PUT, "/form").hasRole(ADMIN_ROLE_NAME)
                .antMatchers(HttpMethod.GET, "/form/**").hasAnyRole(ADMIN_ROLE_NAME, PARTICIPANT_ROLE_NAME)
                //note controller
                .antMatchers(HttpMethod.GET, "/note/**").hasAnyRole(ADMIN_ROLE_NAME, PARTICIPANT_ROLE_NAME)
                .antMatchers(HttpMethod.POST, "/note").hasRole(ADMIN_ROLE_NAME)
                .antMatchers(HttpMethod.PUT, "/note").hasRole(ADMIN_ROLE_NAME)
                .antMatchers(HttpMethod.DELETE, "/note/**").hasRole(ADMIN_ROLE_NAME)
                .antMatchers(HttpMethod.POST, "/note/tag/add").hasRole(ADMIN_ROLE_NAME)
                .antMatchers(HttpMethod.GET, "/note/tag/**").hasAnyRole(ADMIN_ROLE_NAME, PARTICIPANT_ROLE_NAME)
                .antMatchers(HttpMethod.DELETE, "/note/**/delete/tag/**").hasRole(ADMIN_ROLE_NAME)
                //roadmap controller
                .antMatchers(HttpMethod.GET, "/roadmap/**").hasAnyRole(ADMIN_ROLE_NAME, PARTICIPANT_ROLE_NAME)
                .antMatchers(HttpMethod.POST, "/roadmap").hasRole(ADMIN_ROLE_NAME)
                .antMatchers(HttpMethod.PUT, "/roadmap").hasRole(ADMIN_ROLE_NAME)
                .antMatchers(HttpMethod.DELETE, "/roadmap/**").hasRole(ADMIN_ROLE_NAME)
                .antMatchers(HttpMethod.POST, "/roadmap/add/event").hasRole(ADMIN_ROLE_NAME)
                .antMatchers(HttpMethod.GET, "/roadmap/**/user/**/statistic/").hasRole(ADMIN_ROLE_NAME)
                //space controller
                .antMatchers(HttpMethod.POST, "/space").hasRole(ADMIN_ROLE_NAME)
                .antMatchers(HttpMethod.GET, "/space/all").hasAnyRole(ADMIN_ROLE_NAME, PARTICIPANT_ROLE_NAME)
                .antMatchers(HttpMethod.PUT, "/space").hasRole(ADMIN_ROLE_NAME)
                .antMatchers(HttpMethod.DELETE, "/space/**").hasRole(ADMIN_ROLE_NAME)
                //task controller
                .antMatchers(HttpMethod.GET, "/task/**").hasAnyRole(ADMIN_ROLE_NAME, PARTICIPANT_ROLE_NAME)
                .antMatchers(HttpMethod.PUT, "/task").hasRole(ADMIN_ROLE_NAME)
                .antMatchers(HttpMethod.DELETE, "/task/**").hasRole(ADMIN_ROLE_NAME)
                .antMatchers(HttpMethod.POST, "/task/tag/add").hasRole(ADMIN_ROLE_NAME)
                .antMatchers(HttpMethod.GET, "/task/tag/**").hasAnyRole(ADMIN_ROLE_NAME, PARTICIPANT_ROLE_NAME)
                .antMatchers(HttpMethod.DELETE, "/task/**/delete/tag/**").hasRole(ADMIN_ROLE_NAME)
                .and()
                .httpBasic()
                .and()
                .csrf().disable();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }
}
