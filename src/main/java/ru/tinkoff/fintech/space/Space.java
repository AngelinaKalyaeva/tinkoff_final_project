package ru.tinkoff.fintech.space;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

/**
 * Space - рабочее пространство.
 */

@Data
@Entity
@Table(name = "space")
@NoArgsConstructor
public class Space {
    @Id
    @Column(name = "space_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "space_name")
    @NotBlank(message = "Space name cannot be blank")
    private String name;

    public Space (String name) {
        this.name = name;
    }
}
