package ru.tinkoff.fintech.space;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/space")
@RequiredArgsConstructor
public class SpaceController {
    private final SpaceService spaceService;

    @PostMapping
    public Space createSpace(@RequestParam @NotBlank String name) {
        return spaceService.createSpace(name);
    }

    @GetMapping("/all")
    public Iterable<Space> getAllSpaces() {
        return spaceService.getAllSpaces();
    }

    @PutMapping
    public Space updateSpace(@RequestBody @Valid Space space) {
        return spaceService.updateSpace(space);
    }

    @DeleteMapping("/{id}")
    public void deleteSpace(@PathVariable("id") @NotNull Long id) {
        spaceService.deleteSpace(id);
    }
}
