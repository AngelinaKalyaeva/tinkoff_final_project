package ru.tinkoff.fintech.space;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpaceRepository extends CrudRepository <Space, Long> {
}
