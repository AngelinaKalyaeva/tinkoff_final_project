package ru.tinkoff.fintech.space;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
public class SpaceService {
    private final SpaceRepository spaceRepository;

    public Space createSpace(String name) {
        return spaceRepository.save(new Space(name));
    }

    public Space getSpaceById(Long id) {
        return spaceRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Space not found by id: " + id));
    }

    public boolean isSpaceNotExist(Long id) {
        return spaceRepository.findById(id).isEmpty();
    }

    public Iterable<Space> getAllSpaces() {
        return spaceRepository.findAll();
    }

    public Space updateSpace(Space space) {
        if (space.getId() == null) {
            throw new IllegalArgumentException("Space with null id cannot be updated");
        }
        return spaceRepository.save(space);
    }

    public void deleteSpace(Long id) {
        spaceRepository.deleteById(id);
    }
}
