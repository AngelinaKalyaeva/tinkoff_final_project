package ru.tinkoff.fintech.status;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * StatusBoard - сущность для связи статуса (status) с доской (board).
 */

@Data
@Entity
@Table(name = "status_board")
@NoArgsConstructor
public class StatusBoard {
    @Id
    @Column(name = "status_board_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "board_id")
    @NotNull
    private Long boardId;

    @Column(name = "status_id")
    private long statusId;

    @Column(name = "status_number")
    private long statusNumber;

    public StatusBoard(Long boardId, Long statusId) {
        this.boardId = boardId;
        this.statusId = statusId;
    }

    public StatusBoard(Long boardId, Long statusId, Long statusNumber) {
        this.boardId = boardId;
        this.statusId = statusId;
        this.statusNumber = statusNumber;
    }
}
