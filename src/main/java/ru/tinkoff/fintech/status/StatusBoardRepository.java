package ru.tinkoff.fintech.status;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StatusBoardRepository extends CrudRepository<StatusBoard, Long> {
    List<StatusBoard> findAllByBoardId(Long id);

    void deleteAllByBoardId(Long id);

    Optional<StatusBoard> findByBoardIdAndStatusId(Long boardId, long statusId);

    @Modifying
    @Query(
            value = """
                    insert into status_board (board_id, status_id, status_number)
                    values (
                        :boardId,
                        :statusId,
                        coalesce((select count(*) from status_board where board_id = :boardId), 0) + 1
                    )
                    """,
            nativeQuery = true)
    void insertStatusBoard(@Param("boardId") Long boardId, @Param("statusId") Long statusId);
}
