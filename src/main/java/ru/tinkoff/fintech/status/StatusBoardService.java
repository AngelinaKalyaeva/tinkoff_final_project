package ru.tinkoff.fintech.status;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
public class StatusBoardService {
    private final StatusBoardRepository statusBoardRepository;

    @Transactional
    public void createStatusBoard(StatusBoard statusBoard) {
        if (statusBoard.getId() != null) {
            throw new IllegalArgumentException("StatusBoard with not null id cannot be created");
        }

        statusBoardRepository.insertStatusBoard(
                statusBoard.getBoardId(),
                statusBoard.getStatusId()
        );
    }

    @Transactional
    public void batchCreateStatusBoards(List<StatusBoard> statusBoards) {
        boolean isStatusBoardWithNotNullId = statusBoards.stream().anyMatch(el -> el.getId() != null);
        if(isStatusBoardWithNotNullId) {
            throw new IllegalArgumentException("StatusBoard with not null id cannot be created.");
        }
        statusBoardRepository.saveAll(statusBoards);
    }

    public List<StatusBoard> getAllStatusBoardByBoardId(Long boarId) {
        return statusBoardRepository.findAllByBoardId(boarId);
    }

    public StatusBoard getStatusBoardByBoardIdAndStatusId(Long boardId, long statusId) {
        return statusBoardRepository.findByBoardIdAndStatusId(boardId, statusId)
                .orElseThrow(() -> new NoSuchElementException(
                        "TagNote not found by board id: " + boardId + "and tag id: " + statusId
                ));
    }

    @Transactional
    public void batchUpdateStatusBoards(List<StatusBoard> statusBoards) {
        boolean isStatusBoardWithNullId = statusBoards.stream().anyMatch(el -> el.getId() == null);
        if(isStatusBoardWithNullId) {
            throw new IllegalArgumentException("StatusBoard with null id cannot be updated.");
        }
        statusBoardRepository.saveAll(statusBoards);
    }

    public void deleteStatusBoard (long id){
        statusBoardRepository.deleteById(id);
    }

    public void deleteAllStatusesFromBoard (long id){
        statusBoardRepository.deleteAllByBoardId(id);
    }
}
