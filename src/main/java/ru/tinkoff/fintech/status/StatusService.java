package ru.tinkoff.fintech.status;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class StatusService {
    private final StatusRepository statusRepository;

    @Transactional
    public Status createOrGetStatus(String statusName) {
        Optional<Status> statusFromDb = statusRepository.findByName(statusName);
        return statusFromDb.orElseGet(() -> statusRepository.save(new Status(statusName)));
    }

    public List<Status> getAllById(List<Long> statusesId) {
        return (List<Status>) statusRepository.findAllById(statusesId);
    }
}
