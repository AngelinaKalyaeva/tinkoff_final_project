package ru.tinkoff.fintech.status.dto;

import lombok.Data;
import ru.tinkoff.fintech.status.StatusBoard;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Data
public class StatusBoardDto {
    @Valid
    private StatusBoard statusBoard;

    @NotBlank(message = "New status name cannot be blank")
    private String newStatusName;
}
