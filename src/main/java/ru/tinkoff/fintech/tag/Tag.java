package ru.tinkoff.fintech.tag;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Tag - элемент для поиска.
 * Тэг (tag) может быть добавлен к заметке (note) или задаче (task).
 */

@Data
@Entity
@Table(name = "tag")
@NoArgsConstructor
public class Tag {
    @Id
    @Column(name = "tag_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "tag_name")
    private String name;

    public Tag (String name) {
        this.name = name;
    }
}
