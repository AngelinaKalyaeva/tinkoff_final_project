package ru.tinkoff.fintech.tag;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TagService {
    private final TagRepository tagRepository;

    @Transactional
    public Tag createTag(String tagName) {
        Optional<Tag> tagFromBd = tagRepository.findByName(tagName);
        return tagFromBd.orElseGet(() -> tagRepository.save(new Tag(tagName)));
    }

    public Tag getTag(String tagName) {
        return tagRepository.findByName(tagName)
                .orElseThrow(() -> new NoSuchElementException("Tag not found by name: " + tagName));
    }
}
