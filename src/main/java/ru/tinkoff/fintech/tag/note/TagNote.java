package ru.tinkoff.fintech.tag.note;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * TagNote - сущность для связи тэга (tag) с заметкой (note).
 */

@Data
@Entity
@Table(name = "tag_note")
@NoArgsConstructor
public class TagNote {
    @Id
    @Column(name = "tag_note_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "note_id")
    private Long noteId;

    @Column(name = "tag_id")
    private Long tagId;

    public TagNote (Long noteId, Long tagId) {
        this.noteId = noteId;
        this.tagId = tagId;
    }
}
