package ru.tinkoff.fintech.tag.note;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TagNoteRepository extends CrudRepository<TagNote, Long> {
    Optional<TagNote> findByNoteIdAndTagId (Long noteId, long tagId);

    List<TagNote> findAllByTagId(long tagId);
}
