package ru.tinkoff.fintech.tag.note;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
public class TagNoteService {
    private final TagNoteRepository tagNoteRepository;

    public void createTagNote(TagNote tagNote) {
        if (tagNote.getId() != null) {
            throw new IllegalArgumentException("TagNote with not null id cannot be created.");
        }
        tagNoteRepository.save(tagNote);
    }

    public TagNote getTagNotByNoteIdAndTagId(Long noteId, long tagId) {
        return tagNoteRepository.findByNoteIdAndTagId(noteId, tagId)
                .orElseThrow(() -> new NoSuchElementException(
                        "TagNote not found by note id: " + noteId + "and tag id: " + tagId
                ));
    }

    public List<Long> getAllNotesIdByTagId(long tagId) {
        return tagNoteRepository.findAllByTagId(tagId)
                .stream()
                .map(TagNote::getNoteId)
                .toList();
    }

    public void deleteTagNote(Long id) {
        tagNoteRepository.deleteById(id);
    }
}
