package ru.tinkoff.fintech.tag.task;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * TagTask - сущность для связи тэга (tag) с задачей (task).
 */

@Data
@Entity
@Table(name = "tag_task")
@NoArgsConstructor
public class TagTask {
    @Id
    @Column(name = "tag_task_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "task_id")
    private Long taskId;

    @Column(name = "tag_id")
    private Long tagId;

    public TagTask (Long taskId, Long tagId) {
        this.taskId = taskId;
        this.tagId = tagId;
    }
}
