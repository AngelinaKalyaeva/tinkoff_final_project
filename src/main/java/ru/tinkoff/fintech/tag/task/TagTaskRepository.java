package ru.tinkoff.fintech.tag.task;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TagTaskRepository extends CrudRepository<TagTask, Long> {
    Optional<TagTask> findByTaskIdAndTagId (Long taskId, long tagId);

    List<TagTask> findAllByTagId(long tagId);

    void deleteAllByTaskId(long taskId);
}
