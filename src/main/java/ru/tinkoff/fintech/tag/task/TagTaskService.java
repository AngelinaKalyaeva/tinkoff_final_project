package ru.tinkoff.fintech.tag.task;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
public class TagTaskService {
    private final TagTaskRepository tagTaskRepository;

    public void createTagTask(TagTask tagTask) {
        if (tagTask.getId() != null) {
            throw new IllegalArgumentException("TagTask with not null id cannot be created.");
        }
        tagTaskRepository.save(tagTask);
    }

    public List<Long> getAllTasksIdByTagId(long tagId) {
        return tagTaskRepository.findAllByTagId(tagId)
                .stream()
                .map(TagTask::getTaskId)
                .toList();
    }

    public void batchDeleteTagTaskByTaskId(long taskId) {
        tagTaskRepository.deleteAllByTaskId(taskId);
    }

    public TagTask getTagNotByTaskIdAndTagId(Long taskId, long tagId) {
        return tagTaskRepository.findByTaskIdAndTagId(taskId, tagId)
                .orElseThrow(() -> new NoSuchElementException(
                        "TagNote not found by note id: " + taskId + "and tag id: " + tagId
                ));
    }

    public void deleteTagTask(Long id) {
        tagTaskRepository.deleteById(id);
    }
}
