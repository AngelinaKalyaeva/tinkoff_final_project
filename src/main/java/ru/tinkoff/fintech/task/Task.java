package ru.tinkoff.fintech.task;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Task - часть доски (board).
 * Задача (task) может быть прикреплена к статусу (status), связвнному с доской (board)
 */

@Data
@Entity
@Table(name = "task")
@EntityListeners(AuditingEntityListener.class)
@Accessors(chain = true)
@NoArgsConstructor
public class Task {
    @Id
    @Column(name = "task_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "board_id")
    @NotNull
    private Long boardId;

    @Column(name = "status_id")
    private long statusId;

    @Column(name = "task_name")
    @NotBlank(message = "Task name cannot be blank")
    private String name;

    @Column(name = "task_description")
    @NotNull
    private String description;

    @Column(name = "created_date", updatable = false)
    @CreatedDate
    private LocalDateTime createdDate;

    @Column(name = "updated_date")
    @LastModifiedDate
    private LocalDateTime updatedDate;

    @Column(name = "assigned_user")
    private String assignedUserName;

    public Task(Task task) {
        this.name = task.name;
        this.boardId = task.boardId;
        this.assignedUserName = task.assignedUserName;
        this.description = task.description;
        this.statusId = task.statusId;
        this.createdDate = task.createdDate;
        this.updatedDate = task.updatedDate;
    }
}
