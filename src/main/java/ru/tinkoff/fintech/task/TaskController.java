package ru.tinkoff.fintech.task;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/task")
@RequiredArgsConstructor
public class TaskController {
    private final TaskService taskService;

    @GetMapping("/{taskId}")
    public Task getTask(@PathVariable("taskId") @NotNull Long taskId) {
        return taskService.getTaskById(taskId);
    }

    @PutMapping
    public Task updateTask(@RequestBody @Valid Task task) {
        return taskService.updateTask(task);
    }

    @DeleteMapping("/{taskId}")
    public void deleteTask(@PathVariable("taskId") @NotNull Long taskId) {
        taskService.deleteTask(taskId);
    }

    @PostMapping("/tag/add")
    public void addTagToTask(@RequestParam @NotNull Long taskId, @RequestParam @NotBlank String tagName) {
        taskService.addTagToTask(taskId, tagName);
    }

    @GetMapping("/tag/{tagName}")
    public List<Long> getTasksByTagName(@PathVariable("tagName") @NotBlank String tagName) {
        return taskService.getAllTasksIdByTagName(tagName);
    }

    @DeleteMapping("/{taskId}/delete/tag/{tagId}")
    public void deleteTagFromTask(@PathVariable("taskId") @NotNull Long taskId, @PathVariable ("tagId") long tagId) {
        taskService.deleteTagFromTask(taskId,tagId);
    }
}
