package ru.tinkoff.fintech.task;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends CrudRepository<Task, Long> {
    List<Task> findAllByBoardId(Long boardId);

    List<Task> findAllByStatusId(Long statusId);

    List<Task> findAllByBoardIdAndStatusId(Long boardId, Long statusId);

    void deleteAllByBoardId(Long boardId);

    void deleteAllByStatusId(Long statusId);
}
