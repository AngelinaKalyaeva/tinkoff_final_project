package ru.tinkoff.fintech.task;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.fintech.tag.Tag;
import ru.tinkoff.fintech.tag.TagService;
import ru.tinkoff.fintech.tag.task.TagTask;
import ru.tinkoff.fintech.tag.task.TagTaskService;

import java.util.List;
import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
public class TaskService {
    private final TaskRepository taskRepository;
    private final TagService tagService;
    private final TagTaskService tagTaskService;

    public Task createTask(Task task) {
        if (task.getId() != null) {
            throw new IllegalArgumentException("Task with not null id cannot be created");
        }
        task.setUpdatedDate(null);
        return taskRepository.save(task);
    }

    @Transactional
    public void batchCreateTasks(List<Task> tasks) {
        boolean isStatusBoardWithNotNullId = tasks.stream().anyMatch(el -> el.getId() != null);
        if(isStatusBoardWithNotNullId) {
            throw new IllegalArgumentException("Task with not null id cannot be created.");
        }
        taskRepository.saveAll(tasks);
    }

    @Transactional
    public void batchUpdateTasks(List<Task> tasks) {
        boolean isStatusBoardWithNullId = tasks.stream().anyMatch(el -> el.getId() == null);
        if(isStatusBoardWithNullId) {
            throw new IllegalArgumentException("Task with null id cannot be updated.");
        }
        taskRepository.saveAll(tasks);
    }

    public Task getTaskById(Long id) {
        return taskRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Task not found by id: " + id));
    }

    public List<Task> getAllTasksByBoardId(Long boardId) {
        return taskRepository.findAllByBoardId(boardId);
    }

    public List<Task> getAllTasksByStatusId(Long statusId) {
        return taskRepository.findAllByStatusId(statusId);
    }

    public List<Task> getAllTasksByBoardIdAndStatusId(Long boardId, Long statusId) {
        return taskRepository.findAllByBoardIdAndStatusId(boardId, statusId);
    }

    @Transactional
    public Task updateTask(Task updatedTask) {
        Long taskId = updatedTask.getId();
        Task actualTask = taskRepository.findById(taskId)
                .orElseThrow(() -> new NoSuchElementException("Task not found by id: " + taskId));
        updatedTask.setCreatedDate(actualTask.getCreatedDate());
        return taskRepository.save(updatedTask);
    }

    @Transactional
    public void deleteTask(Long id) {
        tagTaskService.batchDeleteTagTaskByTaskId(id);
        taskRepository.deleteById(id);
    }

    @Transactional
    public void batchDeleteTasksByBoardId(Long boardId) {
        getAllTasksByBoardId(boardId).stream()
                .map(Task::getId)
                .forEach(tagTaskService::batchDeleteTagTaskByTaskId);

        taskRepository.deleteAllByBoardId(boardId);
    }

    @Transactional
    public void batchDeleteTasksByStatusId(Long statusId) {
        getAllTasksByStatusId(statusId).stream()
                .map(Task::getId)
                .forEach(tagTaskService::batchDeleteTagTaskByTaskId);

        taskRepository.deleteAllByStatusId(statusId);
    }

    @Transactional
    public void addTagToTask(Long taskId, String tagName) {
        if (isTaskNotExist(taskId)) {
            throw new IllegalArgumentException("Task with id: " + taskId + "not found");
        }
        Long tagId = tagService.createTag(tagName).getId();
        TagTask tagTask = new TagTask(taskId, tagId);
        tagTaskService.createTagTask(tagTask);
    }

    @Transactional
    public List<Long> getAllTasksIdByTagName(String tagName) {
        Tag tag = tagService.getTag(tagName);
        return tagTaskService.getAllTasksIdByTagId(tag.getId());
    }

    @Transactional
    public void deleteTagFromTask(Long taskId, long tagId) {
        Long tagTaskId = tagTaskService.getTagNotByTaskIdAndTagId(taskId, tagId).getId();
        tagTaskService.deleteTagTask(tagTaskId);
    }

    private boolean isTaskNotExist(Long id) {
        return taskRepository.findById(id).isEmpty();
    }
}
