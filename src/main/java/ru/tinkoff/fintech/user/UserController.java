package ru.tinkoff.fintech.user;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.fintech.user.dto.UserDto;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @PostMapping("/registration")
    public void createUser(@RequestBody @Valid UserDto userDto) {
        userService.createUserWithRole( userDto.getUsername(), userDto.getName(), userDto.getPassword());
    }
}
