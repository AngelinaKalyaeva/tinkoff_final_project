package ru.tinkoff.fintech.user;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, String> {
    @Modifying
    @Query(
            value = "insert into users values (:username, :name, :password, :roleId)",
            nativeQuery = true
    )
    void insertUser(
            @Param("username") String username,
            @Param("name") String name,
            @Param("password") String password,
            @Param("roleId") long roleId
    );
}
