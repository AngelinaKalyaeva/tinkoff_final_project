package ru.tinkoff.fintech.user;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.fintech.role.RoleService;

import static ru.tinkoff.fintech.security.SecurityConfiguration.PARTICIPANT_ROLE_NAME;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleService roleService;

    public User getUserById(String username) {
        return userRepository.findById(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found by username: " + username));
    }

    @Transactional
    public void createUserWithRole(String username, String name, String password) {
        long roleId = roleService.getRoleByName(PARTICIPANT_ROLE_NAME).getId();
        userRepository.insertUser(
                username,
                name,
                passwordEncoder.encode(password),
                roleId
        );
    }
}
