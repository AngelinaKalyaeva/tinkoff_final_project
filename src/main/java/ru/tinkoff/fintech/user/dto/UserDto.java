package ru.tinkoff.fintech.user.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
public class UserDto {
    private static final String EMAIL_REGEXP = "^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";
    private static final String PASSWORD_REGEXP = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$";

    @Pattern(regexp = EMAIL_REGEXP)
    private String username;

    @NotBlank(message = "Name cannot be blank")
    private String name;

    @Pattern(
            regexp = PASSWORD_REGEXP,
            message = """
                    Must have at least one numeric character
                    Must have at least one lowercase character
                    Must have at least one uppercase character
                    Must have at least one special symbol among @#$%
                    Password length should be  more than 8
                    """
    )
    private String password;
}
