create table if not exists role (
    role_id serial primary key,
    role_name varchar(50) not null
);

create table if not exists users (
    username varchar(30) primary key,
    full_name varchar(30) not null,
    password varchar(100) not null,
    role_id bigint not null,
    foreign key (role_id) references role(role_id)
);

create table if not exists space (
    space_id serial primary key,
    space_name varchar(50) not null
);

create table if not exists form_type (
    type_id serial primary key,
    type_name varchar(50) not null
);

create table if not exists form (
    form_id serial primary key,
    space_id bigint not null,
    type_id bigint not null,
    x_coordinate real not null,
    y_coordinate real not null,
    height real not null,
    length real not null,
    foreign key (space_id) references space(space_id),
    foreign key (type_id) references form_type(type_id)
);

create table if not exists board (
    board_id bigint primary key,
    board_name varchar(50) not null,
    board_description varchar(255) not null,
    created_date timestamp not null,
    updated_date timestamp,
    foreign key (board_id) references form(form_id)
);

create table if not exists status (
    status_id serial primary key,
    status_name varchar(50) unique not null
);

create table if not exists status_board (
    status_board_id bigserial primary key,
    board_id bigint not null,
    status_id bigint not null,
    status_number serial not null,
    unique (board_id, status_id),
    foreign key (board_id) references board(board_id),
    foreign key (status_id) references status(status_id)
);

create table if not exists task (
    task_id serial primary key,
    board_id bigint not null,
    status_id bigint not null,
    task_name varchar(50) not null,
    task_description varchar(255) not null,
    created_date timestamp not null,
    updated_date timestamp,
    assigned_user varchar(30),
    foreign key (board_id) references board(board_id),
    foreign key (status_id) references status(status_id),
    foreign key (assigned_user) references users(username)
);

create table if not exists tag (
    tag_id serial primary key,
    tag_name varchar(50) unique not null
);

create table if not exists tag_task (
    tag_task_id serial primary key,
    task_id bigint not null,
    tag_id bigint not null,
    foreign key (task_id) references task(task_id),
    foreign key (tag_id) references tag(tag_id)
);

create table if not exists note (
    note_id bigint primary key,
    note_name varchar(50) not null,
    text varchar(255) not null,
    created_date timestamp not null,
    updated_date timestamp,
    linked_note_id bigint,
    foreign key (note_id) references form(form_id)
);

create table if not exists tag_note (
    tag_note_id serial primary key,
    note_id bigint not null,
    tag_id bigint not null,
    foreign key (note_id) references note(note_id),
    foreign key (tag_id) references tag(tag_id)
);

create table if not exists roadmap (
    roadmap_id bigint primary key,
    roadmap_name varchar(50) not null,
    start_date timestamp not null,
    end_date timestamp not null,
    foreign key (roadmap_id) references form(form_id)
);

create table if not exists event (
    event_id serial primary key,
    roadmap_id bigint not null,
    event_name varchar(50) not null,
    short_description varchar(127) not null,
    planned_start_date timestamp not null,
    planned_end_date timestamp not null,
    actual_start_date timestamp,
    actual_end_date timestamp,
    assigned_user varchar(30),
    foreign key (roadmap_id) references roadmap (roadmap_id),
    foreign key (assigned_user) references users(username)
);

insert into role (role_name) values ('admin');
insert into role (role_name) values  ('participant');
insert into users select 'admin', 'admin', '$2a$10$RS4627Urgvjhpogeiysjfe.TQYgWSGqVqvkhKRmeSvHV9T5Cfzi52', role_id from role where role_name = 'admin';

insert into form_type (type_name) values ('BOARD');
insert into form_type (type_name) values ('NOTE');
insert into form_type (type_name) values ('ROADMAP');
