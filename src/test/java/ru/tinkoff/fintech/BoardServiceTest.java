package ru.tinkoff.fintech;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import ru.tinkoff.fintech.board.Board;
import ru.tinkoff.fintech.board.BoardRepository;
import ru.tinkoff.fintech.board.BoardService;
import ru.tinkoff.fintech.board.dto.BoardDto;
import ru.tinkoff.fintech.form.Form;
import ru.tinkoff.fintech.form.FormService;
import ru.tinkoff.fintech.status.Status;
import ru.tinkoff.fintech.status.StatusBoard;
import ru.tinkoff.fintech.status.StatusBoardService;
import ru.tinkoff.fintech.status.StatusService;
import ru.tinkoff.fintech.task.Task;
import ru.tinkoff.fintech.task.TaskService;

import javax.persistence.EntityNotFoundException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

class BoardServiceTest {
	private final BoardRepository boardRepository = Mockito.mock(BoardRepository.class);
	private final FormService formService = Mockito.mock(FormService.class);
	private final StatusService statusService = Mockito.mock(StatusService.class);
	private final StatusBoardService statusBoardService = Mockito.mock(StatusBoardService.class);
	private final TaskService taskService = Mockito.mock(TaskService.class);

	private final BoardService boardService = new BoardService(
			boardRepository,
			formService,
			statusService,
			statusBoardService,
			taskService
	);

	@Test
	void getBoardByIdTestSuccessfully() {
		Long boardId = Instant.now().getEpochSecond();
		Board board = new Board();
		board.setId(boardId);

		Form form = new Form();
		form.setId(boardId);

		List<StatusBoard> statusBoards = new ArrayList<>();
		statusBoards.add(new StatusBoard(boardId, 1L));
		statusBoards.add(new StatusBoard(boardId, 2L));

		List<Status> statuses = new ArrayList<>();
		statuses.add(new Status(1L, "TO DO"));
		statuses.add(new Status(2L, "DONE"));

		List<Task> tasks = new ArrayList<>();
		tasks.add(new Task().setId(Instant.now().getEpochSecond()).setBoardId(boardId).setStatusId(1L));
		tasks.add(new Task().setId(Instant.now().getEpochSecond()).setBoardId(boardId).setStatusId(1L));
		tasks.add(new Task().setId(Instant.now().getEpochSecond()).setBoardId(boardId).setStatusId(2L));
		tasks.add(new Task().setId(Instant.now().getEpochSecond()).setBoardId(boardId).setStatusId(2L));

		Mockito.when(boardRepository.findById(boardId)).thenReturn(Optional.of(board));
		Mockito.when(formService.getForm(boardId)).thenReturn(form);
		Mockito.when(statusBoardService.getAllStatusBoardByBoardId(boardId)).thenReturn(statusBoards);
		Mockito.when(statusService.getAllById(Mockito.anyList())).thenReturn(statuses);
		Mockito.when(taskService.getAllTasksByBoardId(boardId)).thenReturn(tasks);

		BoardDto boardById = boardService.getBoardById(boardId);

		Assertions.assertEquals(boardId, boardById.getBoard().getId());
		Assertions.assertEquals(boardId, boardById.getForm().getId());
		Assertions.assertEquals(statuses, boardById.getStatuses());
		Assertions.assertEquals(2L, boardById.getTasksByStatusId().keySet().size());

		int tasksSize = boardById.getTasksByStatusId().values()
				.stream()
				.flatMap(List::stream)
				.toList()
				.size();
		Assertions.assertEquals(4L, tasksSize);
	}

	@Test
	void getBoardByIdTestWithWrongBoardId() {
		Long boardId = Instant.now().getEpochSecond();
		Board board = new Board();
		board.setId(boardId);

		Mockito.when(boardRepository.findById(boardId)).thenReturn(Optional.empty());

		Assertions.assertThrows(
				EntityNotFoundException.class,
				() -> boardService.getBoardById(boardId)
		);
	}

	@Test
	void createBoard() {
		Mockito.when(boardRepository.save(Mockito.any())).thenAnswer((Answer<Board>) invocation -> {
			Object[] args = invocation.getArguments();
			return (Board) args[0];
		});

		Long boardId = Instant.now().getEpochSecond();

		Board board = new Board();
		board.setId(boardId);

		Form form = new Form();
		form.setId(boardId);

		Mockito.when(formService.createForm(form)).thenReturn(form);

		Board createdBoard = boardService.createBoard(board, form);

		Assertions.assertNull(createdBoard.getUpdatedDate());
	}

	@Test
	void updateBoard() {
		Mockito.when(boardRepository.save(Mockito.any())).thenAnswer((Answer<Board>) invocation -> {
			Object[] args = invocation.getArguments();
			return (Board) args[0];
		});

		Long boardId = Instant.now().getEpochSecond();

		LocalDateTime createDate = LocalDateTime.now();
		Board board = new Board();
		board.setId(boardId);
		board.setCreatedDate(createDate);

		Board boardForUpdate = new Board();
		boardForUpdate.setId(boardId);
		boardForUpdate.setCreatedDate(LocalDateTime.now());

		Form form = new Form();
		form.setId(boardId);

		Mockito.when(formService.createForm(form)).thenReturn(form);
		Mockito.when(boardRepository.findById(boardId)).thenReturn(Optional.of(board));

		Board updatedBoard = boardService.updateBoard(boardForUpdate);

		Assertions.assertEquals(createDate, updatedBoard.getCreatedDate());
	}

	@Test
	void updateBoardWithNullId() {
		Board boardForUpdate = new Board();

		Assertions.assertThrows(
				IllegalArgumentException.class,
				() -> boardService.updateBoard(boardForUpdate)
		);
	}

	@Test
	void migrateBoard() {
		Long boardId = Instant.now().getEpochSecond();
		Board board = new Board();
		board.setId(boardId);

		Form form = new Form();
		form.setId(boardId);

		List<StatusBoard> statusBoards = new ArrayList<>();
		statusBoards.add(new StatusBoard(boardId, 1L));
		statusBoards.add(new StatusBoard(boardId, 2L));

		List<Task> tasks = new ArrayList<>();
		tasks.add(new Task().setId(Instant.now().getEpochSecond()).setBoardId(boardId).setStatusId(1L));
		tasks.add(new Task().setId(Instant.now().getEpochSecond()).setBoardId(boardId).setStatusId(1L));
		tasks.add(new Task().setId(Instant.now().getEpochSecond()).setBoardId(boardId).setStatusId(2L));
		tasks.add(new Task().setId(Instant.now().getEpochSecond()).setBoardId(boardId).setStatusId(2L));

		Long targetBoardId = Instant.now().getEpochSecond() + 100;
		Board targetBoard = new Board();
		targetBoard.setId(targetBoardId);
		List<StatusBoard> statusBoardsToMigrate = new ArrayList<>();
		statusBoardsToMigrate.add(new StatusBoard(targetBoardId, 1L));
		statusBoardsToMigrate.add(new StatusBoard(targetBoardId, 2L));

		List<Task> tasksToMigrate = tasks.stream()
				.map(el -> el.setBoardId(targetBoardId))
				.toList();

		Mockito.when(boardRepository.findById(boardId)).thenReturn(Optional.of(board));
		Mockito.when(boardRepository.findById(targetBoardId)).thenReturn(Optional.of(targetBoard));
		Mockito.when(statusBoardService.getAllStatusBoardByBoardId(boardId)).thenReturn(statusBoards);
		Mockito.when(statusBoardService.getAllStatusBoardByBoardId(targetBoardId)).thenReturn(Collections.emptyList());
		Mockito.when(taskService.getAllTasksByBoardId(boardId)).thenReturn(tasks);
		Mockito.doNothing().when(taskService).batchCreateTasks(Mockito.eq(tasksToMigrate));
		Mockito.doNothing().when(statusBoardService).batchCreateStatusBoards(statusBoardsToMigrate);

		boardService.migrateBoard(boardId, targetBoardId);

		Mockito.verify(statusBoardService, Mockito.times(1)).batchCreateStatusBoards(Mockito.eq(statusBoardsToMigrate));
		Mockito.verify(taskService, Mockito.times(1)).batchCreateTasks(Mockito.anyList());
	}
}
