package ru.tinkoff.fintech;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import ru.tinkoff.fintech.event.Event;
import ru.tinkoff.fintech.event.EventRepository;
import ru.tinkoff.fintech.event.EventService;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class EventServiceTest {
    private EventRepository eventRepository;
    private EventService eventService;

    @BeforeEach
    public void init() {
        eventRepository = Mockito.mock(EventRepository.class);
        eventService = new EventService(eventRepository);

        Mockito.when(eventRepository.save(Mockito.any())).thenAnswer((Answer<Event>) invocation -> {
            Object[] args = invocation.getArguments();
            return (Event) args[0];
        });
    }

    @Test
    void createEventSuccessfully() {
        Event event = new Event();
        event.setPlannedEndDate(LocalDateTime.now().plus(1, ChronoUnit.DAYS));
        event.setPlannedStartDate(LocalDateTime.now());

        eventService.createEvent(event);

        Mockito.verify(eventRepository, Mockito.times(1)).save(Mockito.eq(event));
    }

    @Test
    void createEventWithWrongDates() {
        Event event = new Event();
        event.setPlannedEndDate(LocalDateTime.now().minus(1, ChronoUnit.DAYS));
        event.setPlannedStartDate(LocalDateTime.now());

        Assertions.assertThrows(IllegalArgumentException.class, () -> eventService.createEvent(event));
    }

    @Test
    void createEventWithActualDates() {
        Event event = new Event();
        event.setPlannedEndDate(LocalDateTime.now().plus(1, ChronoUnit.DAYS));
        event.setPlannedStartDate(LocalDateTime.now());
        event.setActualEndDate(LocalDateTime.now());

        Assertions.assertThrows(IllegalArgumentException.class, () -> eventService.createEvent(event));
    }

    @Test
    void updateEventSuccessfully() {
        Event event = new Event();
        event.setId(Instant.now().getEpochSecond());
        event.setPlannedEndDate(LocalDateTime.now().plus(1, ChronoUnit.DAYS));
        event.setPlannedStartDate(LocalDateTime.now());
        event.setActualStartDate(LocalDateTime.now());
        event.setActualEndDate(LocalDateTime.now().plus(1, ChronoUnit.DAYS));

        eventService.updateEvent(event);

        Mockito.verify(eventRepository, Mockito.times(1)).save(Mockito.eq(event));
    }

    @Test
    void updateEventWithNullId() {
        Event event = new Event();
        event.setPlannedEndDate(LocalDateTime.now().plus(1, ChronoUnit.DAYS));
        event.setPlannedStartDate(LocalDateTime.now());
        event.setActualStartDate(LocalDateTime.now());
        event.setActualEndDate(LocalDateTime.now().plus(1, ChronoUnit.DAYS));

        Assertions.assertThrows(IllegalArgumentException.class, () -> eventService.updateEvent(event));
    }

    @Test
    void updateEventWithActualStartDateNullAndEndDateNotNull() {
        Event event = new Event();
        event.setId(Instant.now().getEpochSecond());
        event.setPlannedEndDate(LocalDateTime.now().plus(1, ChronoUnit.DAYS));
        event.setPlannedStartDate(LocalDateTime.now());
        event.setActualEndDate(LocalDateTime.now().plus(1, ChronoUnit.DAYS));

        Assertions.assertThrows(IllegalArgumentException.class, () -> eventService.updateEvent(event));
    }

    @Test
    void updateEventWithWrongActualDates() {
        Event event = new Event();
        event.setId(Instant.now().getEpochSecond());
        event.setPlannedEndDate(LocalDateTime.now().plus(1, ChronoUnit.DAYS));
        event.setPlannedStartDate(LocalDateTime.now());
        event.setActualEndDate(LocalDateTime.now().minus(1, ChronoUnit.DAYS));

        Assertions.assertThrows(IllegalArgumentException.class, () -> eventService.updateEvent(event));
    }
}
