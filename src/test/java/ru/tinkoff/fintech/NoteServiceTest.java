package ru.tinkoff.fintech;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import ru.tinkoff.fintech.form.Form;
import ru.tinkoff.fintech.form.FormService;
import ru.tinkoff.fintech.note.Note;
import ru.tinkoff.fintech.note.NoteRepository;
import ru.tinkoff.fintech.note.NoteService;
import ru.tinkoff.fintech.tag.TagService;
import ru.tinkoff.fintech.tag.note.TagNoteService;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class NoteServiceTest {
    private final NoteRepository noteRepository = Mockito.mock(NoteRepository.class);
    private final FormService formService = Mockito.mock(FormService.class);
    private final TagService tagService = Mockito.mock(TagService.class);
    private final TagNoteService tagNoteService = Mockito.mock(TagNoteService.class);

    private final NoteService noteService = new NoteService(
            noteRepository,
            formService,
            tagService,
            tagNoteService
    );

    @Test
    void createNoteSuccessfully() {
        Long formId = Instant.now().getEpochSecond();
        Form form = new Form();
        form.setId(formId);
        form.setTypeId(1);

        Note note = new Note();
        note.setName("name");

        when(formService.createForm(any())).thenAnswer((Answer<Form>) invocation -> {
            Object[] args = invocation.getArguments();
            return (Form) args[0];
        });

        when(noteRepository.save(any())).thenAnswer((Answer<Note>) invocation -> {
            Object[] args = invocation.getArguments();
            return (Note) args[0];
        });

        Note createdNote = noteService.createNote(note, form);

        Assertions.assertEquals(formId, createdNote.getId());
        Assertions.assertNull(note.getUpdatedDate());
    }

    @Test
    void updateNoteSuccessfully() {
        Long noteId = Instant.now().getEpochSecond();
        Long linkedNoteId = Instant.now().getEpochSecond() + 100;

        Note note = new Note();
        note.setId(noteId);
        note.setLinkedNoteId(linkedNoteId);
        note.setName("name");
        LocalDateTime createTime = LocalDateTime.now();
        note.setCreatedDate(createTime);

        when(noteRepository.findById(eq(linkedNoteId))).thenReturn(Optional.of(note));
        when(noteRepository.findById(eq(noteId))).thenReturn(Optional.of(note));

        when(noteRepository.save(any())).thenAnswer((Answer<Note>) invocation -> {
            Object[] args = invocation.getArguments();
            return (Note) args[0];
        });

        Note updatedNote = noteService.updateNote(note);

        Assertions.assertEquals(noteId, updatedNote.getId());
        Assertions.assertEquals(createTime, updatedNote.getCreatedDate());
    }

    @Test
    void updateNoteWithWrongLinkedId() {
        Long noteId = Instant.now().getEpochSecond();
        Long linkedNoteId = Instant.now().getEpochSecond();

        Note note = new Note();
        note.setId(noteId);
        note.setName("name");
        note.setLinkedNoteId(linkedNoteId);
        LocalDateTime createTime = LocalDateTime.now();
        note.setCreatedDate(createTime);

        when(noteRepository.findById(eq(linkedNoteId))).thenReturn(Optional.empty());

        Assertions.assertThrows(IllegalArgumentException.class, () -> noteService.updateNote(note));
    }
}
