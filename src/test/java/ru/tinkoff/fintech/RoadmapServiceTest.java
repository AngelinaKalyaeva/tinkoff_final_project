package ru.tinkoff.fintech;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import ru.tinkoff.fintech.event.Event;
import ru.tinkoff.fintech.event.EventService;
import ru.tinkoff.fintech.form.FormService;
import ru.tinkoff.fintech.roadmap.Roadmap;
import ru.tinkoff.fintech.roadmap.RoadmapRepository;
import ru.tinkoff.fintech.roadmap.RoadmapService;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class RoadmapServiceTest {
    private final RoadmapRepository roadMapRepository = Mockito.mock(RoadmapRepository.class);
    private final FormService formService = Mockito.mock(FormService.class);
    private final EventService eventService = Mockito.mock(EventService.class);

    private final RoadmapService roadmapService = new RoadmapService(
        roadMapRepository,
        formService,
        eventService
    );

    @Test
    void addEventToRoadmapSuccessfully() {
        Long eventId = Instant.now().getEpochSecond();
        Long roadmapId = Instant.now().getEpochSecond();
        Event event = new Event();
        event.setId(eventId);
        event.setRoadmapId(roadmapId);
        event.setPlannedStartDate(LocalDateTime.now());
        event.setPlannedEndDate(LocalDateTime.now().plusDays(1));

        Roadmap roadmap = new Roadmap();
        roadmap.setId(roadmapId);
        roadmap.setStartDate(LocalDateTime.now().minusDays(1));
        roadmap.setEndDate(LocalDateTime.now().plusDays(7));

        Mockito.when(roadMapRepository.findById(eq(roadmapId))).thenReturn(Optional.of(roadmap));

        when(eventService.createEvent(any())).thenAnswer((Answer<Event>) invocation -> {
            Object[] args = invocation.getArguments();
            return (Event) args[0];
        });

        Event createdEvent = roadmapService.addEventToRoadmap(event);

        Assertions.assertEquals(event, createdEvent);
    }

    @Test
    void addEventToRoadmapWithWrongDates() {
        Long eventId = Instant.now().getEpochSecond();
        Long roadmapId = Instant.now().getEpochSecond();
        Event event = new Event();
        event.setId(eventId);
        event.setRoadmapId(roadmapId);
        event.setPlannedStartDate(LocalDateTime.now());
        event.setPlannedEndDate(LocalDateTime.now().plusDays(1));

        Roadmap roadmap = new Roadmap();
        roadmap.setId(roadmapId);
        roadmap.setStartDate(LocalDateTime.now().plusDays(1));
        roadmap.setEndDate(LocalDateTime.now().plusDays(7));

        Mockito.when(roadMapRepository.findById(eq(roadmapId))).thenReturn(Optional.of(roadmap));

        when(eventService.createEvent(any())).thenAnswer((Answer<Event>) invocation -> {
            Object[] args = invocation.getArguments();
            return (Event) args[0];
        });

        Assertions.assertThrows(IllegalArgumentException.class, () -> roadmapService.addEventToRoadmap(event));
    }

    @Test
    void getEventSuccessRateByUser() {
        Long roadmapId = Instant.now().getEpochSecond();

        Event event1 = new Event();
        String username = "user";
        event1.setAssignedUsername(username);
        event1.setPlannedStartDate(LocalDateTime.now().minusDays(1));
        event1.setPlannedEndDate(LocalDateTime.now().plusDays(1));
        event1.setActualStartDate(LocalDateTime.now());
        event1.setActualEndDate(LocalDateTime.now().plusHours(1));

        Event event2 = new Event();
        event2.setAssignedUsername(username);
        event2.setPlannedStartDate(LocalDateTime.now().minusDays(1));
        event2.setPlannedEndDate(LocalDateTime.now().plusDays(1));
        event2.setActualStartDate(LocalDateTime.now());
        event2.setActualEndDate(LocalDateTime.now().plusDays(2));

        Event event3 = new Event();
        event3.setAssignedUsername(username);
        event3.setPlannedStartDate(LocalDateTime.now().minusDays(1));
        event3.setPlannedEndDate(LocalDateTime.now().plusDays(1));
        event3.setActualStartDate(LocalDateTime.now());
        event3.setActualEndDate(LocalDateTime.now().plusHours(5));

        Event event4 = new Event();
        event4.setAssignedUsername(username);
        event4.setPlannedStartDate(LocalDateTime.now());
        event4.setPlannedEndDate(LocalDateTime.now().plusDays(1));
        event4.setActualStartDate(LocalDateTime.now());
        event4.setActualEndDate(LocalDateTime.now().plusHours(2));

        Event event5 = new Event();
        event5.setAssignedUsername(username);
        event5.setPlannedStartDate(LocalDateTime.now());
        event5.setPlannedEndDate(LocalDateTime.now().plusDays(1));
        event5.setActualStartDate(LocalDateTime.now());

        Event event6 = new Event();
        event6.setAssignedUsername("notuser");

        List<Event> events = new ArrayList<>(
                Arrays.asList(event1, event2, event3, event4, event5, event6)
        );

        when(eventService.getAllByRoadmapId(eq(roadmapId))).thenReturn(events);

        double eventSuccessRateByUser = roadmapService.getEventSuccessRateByUser(roadmapId, username);

        Assertions.assertEquals(0.6, eventSuccessRateByUser);
    }
}
