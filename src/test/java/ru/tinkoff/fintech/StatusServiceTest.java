package ru.tinkoff.fintech;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import ru.tinkoff.fintech.status.Status;
import ru.tinkoff.fintech.status.StatusRepository;
import ru.tinkoff.fintech.status.StatusService;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class StatusServiceTest {
    private StatusRepository statusRepository;
    private StatusService statusService;

    @BeforeEach
    public void init() {
        statusRepository = Mockito.mock(StatusRepository.class);
        statusService = new StatusService(statusRepository);

        when(statusRepository.save(any())).thenAnswer((Answer<Status>) invocation -> {
            Object[] args = invocation.getArguments();
            return (Status) args[0];
        });
    }

    @Test
    void createOrGetStatusWhenStatusNotFound() {
        String statusName = "status";

        Mockito.when(statusRepository.findByName(statusName)).thenReturn(Optional.empty());

        statusService.createOrGetStatus(statusName);

        verify(statusRepository, times(1)).save(any());
    }

    @Test
    void createOrGetStatusWithExistStatus() {
        String statusName = "status";
        Status status = new Status(statusName);
        status.setId(1L);
        Mockito.when(statusRepository.findByName(statusName)).thenReturn(Optional.of(status));

        Status orGetStatus = statusService.createOrGetStatus(statusName);

        verify(statusRepository, times(0)).save(any());
        Assertions.assertEquals(status, orGetStatus);
    }
}
