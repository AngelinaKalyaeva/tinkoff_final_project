package ru.tinkoff.fintech;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import ru.tinkoff.fintech.tag.Tag;
import ru.tinkoff.fintech.tag.TagRepository;
import ru.tinkoff.fintech.tag.TagService;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class TagServiceTest {
    private TagRepository tagRepository;
    private TagService tagService;

    @BeforeEach
    public void init() {
        tagRepository = Mockito.mock(TagRepository.class);
        tagService = new TagService(tagRepository);

        when(tagRepository.save(any())).thenAnswer((Answer<Tag>) invocation -> {
            Object[] args = invocation.getArguments();
            return (Tag) args[0];
        });
    }

    @Test
    void createOrGetTagWhenSTagNotFound() {
        String tagName = "tag";

        Mockito.when(tagRepository.findByName(tagName)).thenReturn(Optional.empty());

        tagService.createTag(tagName);

        verify(tagRepository, times(1)).save(any());
    }

    @Test
    void createOrGetStatusWithExistTag() {
        String tagName = "tag";
        Tag tag = new Tag(tagName);
        tag.setId(1L);
        Mockito.when(tagRepository.findByName(tagName)).thenReturn(Optional.of(tag));

        Tag orGetTag = tagService.createTag(tagName);

        verify(tagRepository, times(0)).save(any());
        Assertions.assertEquals(tag, orGetTag);
    }
}
