package ru.tinkoff.fintech;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import ru.tinkoff.fintech.tag.TagService;
import ru.tinkoff.fintech.tag.task.TagTaskService;
import ru.tinkoff.fintech.task.Task;
import ru.tinkoff.fintech.task.TaskRepository;
import ru.tinkoff.fintech.task.TaskService;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class TaskServiceTest {
    private TaskRepository taskRepository;
    private TaskService taskService;

    @BeforeEach
    public void init() {
        taskRepository = Mockito.mock(TaskRepository.class);
        TagService tagService = Mockito.mock(TagService.class);
        TagTaskService tagTaskService = Mockito.mock(TagTaskService.class);

        taskService = new TaskService(
                taskRepository,
                tagService,
                tagTaskService
        );

        when(taskRepository.save(any())).thenAnswer((Answer<Task>) invocation -> {
            Object[] args = invocation.getArguments();
            return (Task) args[0];
        });
    }

    @Test
    void createTaskSuccessfully() {
        Task task = new Task();
        task.setName("task");
        task.setUpdatedDate(LocalDateTime.now());

        Task createdTask = taskService.createTask(task);

        Assertions.assertNull(createdTask.getUpdatedDate());
    }

    @Test
    void updateTaskSuccessfully() {
        Long taskId = Instant.now().getEpochSecond();
        Task task = new Task();
        task.setId(taskId);
        task.setName("task");
        LocalDateTime createDate = LocalDateTime.now();
        task.setCreatedDate(createDate);

        Mockito.when(taskRepository.findById(eq(taskId))).thenReturn(Optional.of(task));

        Task updatedTask = taskService.updateTask(task);

        Assertions.assertEquals(createDate, updatedTask.getCreatedDate());
    }

    @Test
    void updateTaskWhenTaskNotFound() {
        Long taskId = Instant.now().getEpochSecond();
        Task task = new Task();
        task.setId(taskId);
        task.setName("task");
        LocalDateTime createDate = LocalDateTime.now();
        task.setCreatedDate(createDate);

        Mockito.when(taskRepository.findById(eq(taskId))).thenReturn(Optional.empty());

        Assertions.assertThrows(NoSuchElementException.class, () -> taskService.updateTask(task));
    }
}
