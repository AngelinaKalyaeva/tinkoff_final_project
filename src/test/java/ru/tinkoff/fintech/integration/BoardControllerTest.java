package ru.tinkoff.fintech.integration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.jdbc.Sql;
import ru.tinkoff.fintech.board.Board;
import ru.tinkoff.fintech.board.BoardRepository;
import ru.tinkoff.fintech.board.dto.BoardDto;
import ru.tinkoff.fintech.form.Form;
import ru.tinkoff.fintech.form.FormRepository;
import ru.tinkoff.fintech.space.Space;
import ru.tinkoff.fintech.space.SpaceRepository;
import ru.tinkoff.fintech.status.Status;
import ru.tinkoff.fintech.status.StatusBoard;
import ru.tinkoff.fintech.status.StatusBoardRepository;
import ru.tinkoff.fintech.status.StatusBoardService;
import ru.tinkoff.fintech.status.StatusService;
import ru.tinkoff.fintech.task.Task;
import ru.tinkoff.fintech.task.TaskRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.tinkoff.fintech.form.FormType.BOARD;

@Sql({"/clear.sql", "/init.sql"})
public class BoardControllerTest extends ApplicationIntegrationTest {

    @Autowired
    private SpaceRepository spaceRepository;

    @Autowired
    private FormRepository formRepository;

    @Autowired
    private BoardRepository boardRepository;

    @Autowired
    private StatusBoardService statusBoardService;

    @Autowired
    private StatusService statusService;

    @Autowired
    private StatusBoardRepository statusBoardRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Test
    public void shouldGetBoardNoAuth() throws Exception {
        Space space = new Space("space");
        Space savedSpace = spaceRepository.save(space);

        Form form = new Form();
        form.setTypeId(BOARD.formId);
        form.setHeight(100);
        form.setLength(100);
        form.setXCoordinate(0);
        form.setYCoordinate(0);
        form.setSpaceId(savedSpace.getId());
        Form savedForm = formRepository.save(form);

        Board board = new Board();
        board.setId(savedForm.getId());
        board.setName("board");
        board.setDescription("description");
        board.setCreatedDate(LocalDateTime.now());

        Board savedBoard = boardRepository.save(board);

        this.mockMvc.perform(
                        get("/board/" + savedBoard.getId())
                )
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void shouldGetBoardSuccessfully() throws Exception {
        Space space = new Space("space");
        Space savedSpace = spaceRepository.save(space);

        Form form = new Form();
        form.setTypeId(BOARD.formId);
        form.setHeight(100);
        form.setLength(100);
        form.setXCoordinate(0);
        form.setYCoordinate(0);
        form.setSpaceId(savedSpace.getId());
        Form savedForm = formRepository.save(form);

        Board board = new Board();
        board.setId(savedForm.getId());
        board.setName("board");
        board.setDescription("description");
        board.setCreatedDate(LocalDateTime.now());
        Board savedBoard = boardRepository.save(board);

        Status orGetStatus = statusService.createOrGetStatus("In progress");
        statusBoardService.createStatusBoard(new StatusBoard(board.getId(), orGetStatus.getId()));

        BoardDto boardDto = new BoardDto(savedBoard, savedForm, new ArrayList<>(List.of(orGetStatus)), new HashMap<>());

        hibernateQueryInterceptor.startQueryCount();
        String content = this.mockMvc.perform(
                        get("/board/" + boardDto.getBoard().getId())
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_participant", "123"))
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();
        Assertions.assertEquals(7L, hibernateQueryInterceptor.getQueryCount());

        BoardDto resultBoardDto = objectMapper.readValue(content, BoardDto.class);
        boardDto.getBoard().setCreatedDate(resultBoardDto.getBoard().getCreatedDate());
        boardDto.getBoard().setUpdatedDate(resultBoardDto.getBoard().getUpdatedDate());
        Assertions.assertEquals(boardDto, resultBoardDto);
    }

    @Test
    public void shouldUpdateBoardSuccessfully() throws Exception {
        Space space = new Space("space");
        Space savedSpace = spaceRepository.save(space);

        Form form = new Form();
        form.setTypeId(BOARD.formId);
        form.setHeight(100);
        form.setLength(100);
        form.setXCoordinate(0);
        form.setYCoordinate(0);
        form.setSpaceId(savedSpace.getId());
        Form savedForm = formRepository.save(form);

        Board board = new Board();
        board.setId(savedForm.getId());
        board.setName("board");
        board.setDescription("description");
        board.setCreatedDate(LocalDateTime.now());
        Board savedBoard = boardRepository.save(board);

        Status orGetStatus = statusService.createOrGetStatus("In progress");
        statusBoardService.createStatusBoard(new StatusBoard(board.getId(), orGetStatus.getId()));

        String newName = "new name";

        hibernateQueryInterceptor.startQueryCount();
        savedBoard.setName(newName);
        String content = objectMapper.writeValueAsString(savedBoard);
        String resultContent = this.mockMvc.perform(
                        put("/board/")
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_participant", "123"))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(content)
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();
        Assertions.assertEquals(4L, hibernateQueryInterceptor.getQueryCount());

        Board resultBoard = objectMapper.readValue(resultContent, Board.class);
        Assertions.assertEquals(newName, resultBoard.getName());
    }

    @Test
    public void shouldDeleteBoardSuccessfully() throws Exception {
        Space space = new Space("space");
        Space savedSpace = spaceRepository.save(space);

        Form form = new Form();
        form.setTypeId(BOARD.formId);
        form.setHeight(100);
        form.setLength(100);
        form.setXCoordinate(0);
        form.setYCoordinate(0);
        form.setSpaceId(savedSpace.getId());
        Form savedForm = formRepository.save(form);

        Board board = new Board();
        board.setId(savedForm.getId());
        board.setName("board");
        board.setDescription("description");
        board.setCreatedDate(LocalDateTime.now());
        statusBoardRepository.deleteAll();
        boardRepository.deleteAll();
        Board savedBoard = boardRepository.save(board);

        Status orGetStatus = statusService.createOrGetStatus("In progress");
        statusBoardService.createStatusBoard(new StatusBoard(board.getId(), orGetStatus.getId()));

        String newName = "new name";

        hibernateQueryInterceptor.startQueryCount();
        savedBoard.setName(newName);
        this.mockMvc.perform(
                        delete("/board/" + savedBoard.getId())
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_admin", "123"))
                )
                .andExpect(status().is2xxSuccessful());
        Assertions.assertEquals(10L, hibernateQueryInterceptor.getQueryCount());

        List<Board> boards = (List<Board>) boardRepository.findAll();
        Assertions.assertTrue(boards.isEmpty());
    }

    @Test
    public void shouldAddBoardToSpaceSuccessfully() throws Exception {
        Space space = new Space("space");
        Space savedSpace = spaceRepository.save(space);

        Form form = new Form();
        form.setTypeId(BOARD.formId);
        form.setHeight(100);
        form.setLength(100);
        form.setXCoordinate(0);
        form.setYCoordinate(0);
        form.setSpaceId(savedSpace.getId());

        Board board = new Board();
        board.setName("board");
        board.setDescription("description");
        board.setCreatedDate(LocalDateTime.now());

        BoardDto boardDto = new BoardDto(board, form, null, null);

        hibernateQueryInterceptor.startQueryCount();
        String requestContent = objectMapper.writeValueAsString(boardDto);
        String content = this.mockMvc.perform(
                        post("/board")
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_admin", "123"))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestContent)
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        Board resultBoard = objectMapper.readValue(content, Board.class);
        Assertions.assertEquals(board.getName(), resultBoard.getName());
        Assertions.assertEquals(7L, hibernateQueryInterceptor.getQueryCount());
    }

    @Test
    public void shouldAddBoardToSpaceForbidden() throws Exception {
        Space space = new Space("space");
        Space savedSpace = spaceRepository.save(space);

        Form form = new Form();
        form.setTypeId(BOARD.formId);
        form.setHeight(100);
        form.setLength(100);
        form.setXCoordinate(0);
        form.setYCoordinate(0);
        form.setSpaceId(savedSpace.getId());

        Board board = new Board();
        board.setName("board");
        board.setDescription("description");
        board.setCreatedDate(LocalDateTime.now());

        BoardDto boardDto = new BoardDto(board, form, null, null);

        String requestContent = objectMapper.writeValueAsString(boardDto);
        this.mockMvc.perform(
                        post("/board")
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_participant", "123"))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestContent)
                )
                .andExpect(status().isForbidden());
    }

    @Test
    public void shouldMigrateBoardSuccessfully() throws Exception {
        Space space = new Space("space");
        Space savedSpace = spaceRepository.save(space);

        taskRepository.deleteAll();
        Form form2 = new Form();
        form2.setTypeId(BOARD.formId);
        form2.setHeight(100);
        form2.setLength(100);
        form2.setXCoordinate(0);
        form2.setYCoordinate(0);
        form2.setSpaceId(savedSpace.getId());
        Form savedForm2 = formRepository.save(form2);

        Board board2 = new Board();
        board2.setId(savedForm2.getId());
        board2.setName("board");
        board2.setDescription("description");
        board2.setCreatedDate(LocalDateTime.now());
        Board savedBoard2 = boardRepository.save(board2);

        Form form = new Form();
        form.setTypeId(BOARD.formId);
        form.setHeight(100);
        form.setLength(100);
        form.setXCoordinate(0);
        form.setYCoordinate(0);
        form.setSpaceId(savedSpace.getId());
        Form savedForm = formRepository.save(form);

        Board board = new Board();
        board.setId(savedForm.getId());
        board.setName("board");
        board.setDescription("description");
        board.setCreatedDate(LocalDateTime.now());
        Board savedBoard = boardRepository.save(board);

        Status orGetStatus = statusService.createOrGetStatus("In progress");
        statusBoardService.createStatusBoard(new StatusBoard(savedBoard.getId(), orGetStatus.getId()));

        Task task = new Task();
        task.setName("task");
        task.setDescription("description");
        task.setBoardId(savedBoard.getId());
        task.setStatusId(orGetStatus.getId());
        taskRepository.save(task);

        hibernateQueryInterceptor.startQueryCount();
        this.mockMvc.perform(
                        put("/board/" + savedBoard.getId() + "/migrate/" + savedBoard2.getId())
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_admin", "123"))
                )
                .andExpect(status().is2xxSuccessful());
        Assertions.assertEquals(12L, hibernateQueryInterceptor.getQueryCount());

        List<Task> taskFromDb = (List<Task>) taskRepository.findAll();
        Assertions.assertEquals(2, taskFromDb.size());
        List<Long> boardsId = taskFromDb.stream().map(Task::getBoardId).toList();
        List<Long> expectedBoardsId = Arrays.asList(savedBoard.getId(), savedBoard2.getId());
        Assertions.assertEquals(expectedBoardsId, boardsId);
    }
}
