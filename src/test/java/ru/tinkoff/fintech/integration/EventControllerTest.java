package ru.tinkoff.fintech.integration;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.jdbc.Sql;
import ru.tinkoff.fintech.event.Event;
import ru.tinkoff.fintech.event.EventRepository;
import ru.tinkoff.fintech.form.Form;
import ru.tinkoff.fintech.form.FormRepository;
import ru.tinkoff.fintech.roadmap.Roadmap;
import ru.tinkoff.fintech.roadmap.RoadmapRepository;
import ru.tinkoff.fintech.space.Space;
import ru.tinkoff.fintech.space.SpaceRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.tinkoff.fintech.form.FormType.ROADMAP;

@Sql({"/clear.sql", "/init.sql"})
public class EventControllerTest extends ApplicationIntegrationTest {
    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private RoadmapRepository roadmapRepository;

    @Autowired
    private FormRepository formRepository;

    @Autowired
    private SpaceRepository spaceRepository;

    @Test
    void updateEventSuccessfully() throws Exception {
        Event savedEvent = prepareEvent();

        LocalDateTime actualStartDate = LocalDateTime.now();
        LocalDateTime actualEndDate = LocalDateTime.now().plusDays(10);

        savedEvent.setActualStartDate(actualStartDate);
        savedEvent.setActualEndDate(actualEndDate);

        String content = objectMapper.writeValueAsString(savedEvent);
        hibernateQueryInterceptor.startQueryCount();
        this.mockMvc.perform(
                        put("/event")
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_admin", "123"))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(content)
                )
                .andExpect(status().is2xxSuccessful());
        Assertions.assertEquals(4L, hibernateQueryInterceptor.getQueryCount());

        Optional<Event> resultEvent = eventRepository.findById(savedEvent.getId());

        Assertions.assertTrue(resultEvent.isPresent());
        Assertions.assertEquals(actualStartDate.toLocalDate(), resultEvent.get().getActualStartDate().toLocalDate());
        Assertions.assertEquals(actualEndDate.toLocalDate(), resultEvent.get().getActualEndDate().toLocalDate());
    }

    @Test
    void deleteEventSuccessfully() throws Exception {
        Event savedEvent = prepareEvent();

        hibernateQueryInterceptor.startQueryCount();
        this.mockMvc.perform(
                        delete("/event/" + savedEvent.getId())
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_admin", "123"))
                )
                .andExpect(status().is2xxSuccessful());
        Assertions.assertEquals(4L, hibernateQueryInterceptor.getQueryCount());

        List<Event> all = (List<Event>) eventRepository.findAll();
        Assertions.assertTrue(all.isEmpty());
    }

    @NotNull
    private Event prepareEvent() {
        Space space = new Space();
        space.setName("name");

        Space savedSpace = spaceRepository.save(space);

        Form form = new Form();
        form.setSpaceId(savedSpace.getId());
        form.setTypeId(ROADMAP.formId);

        Form savedForm = formRepository.save(form);

        Roadmap roadmap = new Roadmap();
        roadmap.setId(savedForm.getId());
        roadmap.setName("name");
        roadmap.setStartDate(LocalDateTime.now().minusDays(10));
        roadmap.setEndDate(LocalDateTime.now().plusDays(20));

        Roadmap savedRoadmap = roadmapRepository.save(roadmap);

        Event event = new Event();
        event.setRoadmapId(savedRoadmap.getId());
        event.setName("event");
        event.setDescription("description");
        event.setPlannedStartDate(LocalDateTime.now());
        event.setPlannedEndDate(LocalDateTime.now().plusDays(10));

        return eventRepository.save(event);
    }
}
