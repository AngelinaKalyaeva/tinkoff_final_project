package ru.tinkoff.fintech.integration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.jdbc.Sql;
import ru.tinkoff.fintech.form.Form;
import ru.tinkoff.fintech.form.FormRepository;
import ru.tinkoff.fintech.form.dto.FormsDto;
import ru.tinkoff.fintech.space.Space;
import ru.tinkoff.fintech.space.SpaceRepository;

import java.util.Arrays;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.tinkoff.fintech.form.FormType.NOTE;
import static ru.tinkoff.fintech.form.FormType.ROADMAP;

@Sql({"/clear.sql", "/init.sql"})
public class FormControllerTest extends ApplicationIntegrationTest {
    @Autowired
    private FormRepository formRepository;

    @Autowired
    private SpaceRepository spaceRepository;

    @Test
    void getAllFormsForSpaceSuccessfully() throws Exception {
        Space space = new Space();
        String spaceName = "space";
        space.setName(spaceName);
        Space savedSpace = spaceRepository.save(space);

        Form form = new Form();
        form.setSpaceId(savedSpace.getId());
        form.setTypeId(ROADMAP.formId);

        Form formTwo = new Form();
        formTwo.setSpaceId(savedSpace.getId());
        formTwo.setTypeId(NOTE.formId);

        formRepository.saveAll(Arrays.asList(form, formTwo));

        hibernateQueryInterceptor.startQueryCount();
        String resultContent = this.mockMvc.perform(
                        get("/form/" + savedSpace.getId())
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_admin", "123"))
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();
        Assertions.assertEquals(4L, hibernateQueryInterceptor.getQueryCount());

        FormsDto formsDto = objectMapper.readValue(resultContent, FormsDto.class);
        Assertions.assertEquals(spaceName, formsDto.getSpace().getName());
        Assertions.assertEquals(2, formsDto.getForms().size());
    }

    @Test
    void updateFormSuccessfully() throws Exception {
        Space space = new Space();
        String spaceName = "space";
        space.setName(spaceName);
        Space savedSpace = spaceRepository.save(space);

        Form form = new Form();
        form.setSpaceId(savedSpace.getId());
        form.setTypeId(ROADMAP.formId);

        Form savedForm = formRepository.save(form);
        int formSize = 100;
        savedForm.setLength(formSize);
        savedForm.setHeight(formSize);

        hibernateQueryInterceptor.startQueryCount();
        String content = objectMapper.writeValueAsString(savedForm);
        String resultContent = this.mockMvc.perform(
                        put("/form")
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_admin", "123"))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(content)
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();
        Assertions.assertEquals(4L, hibernateQueryInterceptor.getQueryCount());

        Form resultForm = objectMapper.readValue(resultContent, Form.class);
        Assertions.assertEquals(formSize, resultForm.getLength());
        Assertions.assertEquals(formSize, resultForm.getHeight());
    }
}
