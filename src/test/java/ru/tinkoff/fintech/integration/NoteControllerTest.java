package ru.tinkoff.fintech.integration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.jdbc.Sql;
import ru.tinkoff.fintech.form.Form;
import ru.tinkoff.fintech.form.FormRepository;
import ru.tinkoff.fintech.note.Note;
import ru.tinkoff.fintech.note.NoteRepository;
import ru.tinkoff.fintech.note.dto.NoteDto;
import ru.tinkoff.fintech.space.Space;
import ru.tinkoff.fintech.space.SpaceRepository;

import java.time.LocalDateTime;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.tinkoff.fintech.form.FormType.NOTE;

@Sql({"/clear.sql", "/init.sql"})
public class NoteControllerTest extends ApplicationIntegrationTest {

    @Autowired
    private NoteRepository noteRepository;

    @Autowired
    private FormRepository formRepository;

    @Autowired
    private SpaceRepository spaceRepository;

    @Test
    public void shouldGetNoteNoAuth() throws Exception {
        Space space = new Space("space");
        Space savedSpace = spaceRepository.save(space);

        Form form = new Form();
        form.setTypeId(NOTE.formId);
        form.setHeight(100);
        form.setLength(100);
        form.setXCoordinate(0);
        form.setYCoordinate(0);
        form.setSpaceId(savedSpace.getId());
        Form savedForm = formRepository.save(form);

        Note note = new Note();
        note.setId(savedForm.getId());
        note.setName("note");
        note.setContent("text");
        note.setCreatedDate(LocalDateTime.now());

        Note savedNote = noteRepository.save(note);

        this.mockMvc.perform(
                        get("/note/" + savedNote.getId())
                )
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void shouldGetNoteSuccessfully() throws Exception {
        Space space = new Space("space");
        Space savedSpace = spaceRepository.save(space);

        Form form = new Form();
        form.setTypeId(NOTE.formId);
        form.setHeight(100);
        form.setLength(100);
        form.setXCoordinate(0);
        form.setYCoordinate(0);
        form.setSpaceId(savedSpace.getId());
        Form savedForm = formRepository.save(form);

        Note note = new Note();
        note.setId(savedForm.getId());
        note.setName("note");
        note.setContent("text");
        note.setCreatedDate(LocalDateTime.now());

        Note savedNote = noteRepository.save(note);
        NoteDto noteDto = new NoteDto(savedNote, savedForm);

        hibernateQueryInterceptor.startQueryCount();
        String content = this.mockMvc.perform(
                        get("/note/" + savedNote.getId())
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_participant", "123"))
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        NoteDto resultNoteDto = objectMapper.readValue(content, NoteDto.class);
        noteDto.getNote().setCreatedDate(resultNoteDto.getNote().getCreatedDate());
        noteDto.getNote().setUpdatedDate(resultNoteDto.getNote().getUpdatedDate());
        Assertions.assertEquals(noteDto, resultNoteDto);
        Assertions.assertEquals(4L, hibernateQueryInterceptor.getQueryCount());
    }

    @Test
    public void shouldAddNoteToSpaceSuccessfully() throws Exception {
        Space space = new Space("space");
        Space savedSpace = spaceRepository.save(space);

        Form form = new Form();
        form.setTypeId(NOTE.formId);
        form.setHeight(100);
        form.setLength(100);
        form.setXCoordinate(0);
        form.setYCoordinate(0);
        form.setSpaceId(savedSpace.getId());

        Note note = new Note();
        note.setName("note");
        note.setContent("text");
        note.setCreatedDate(LocalDateTime.now());

        NoteDto noteDto = new NoteDto(note, form);

        hibernateQueryInterceptor.startQueryCount();
        String requestContent = objectMapper.writeValueAsString(noteDto);
        String content = this.mockMvc.perform(
                        post("/note")
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_admin", "123"))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestContent)
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        Note resultNote = objectMapper.readValue(content, Note.class);
        Assertions.assertEquals(note.getName(), resultNote.getName());
        Assertions.assertEquals(7L, hibernateQueryInterceptor.getQueryCount());
    }

    @Test
    public void shouldAddNoteToSpaceForbidden() throws Exception {
        Space space = new Space("space");
        Space savedSpace = spaceRepository.save(space);

        Form form = new Form();
        form.setTypeId(NOTE.formId);
        form.setHeight(100);
        form.setLength(100);
        form.setXCoordinate(0);
        form.setYCoordinate(0);
        form.setSpaceId(savedSpace.getId());

        Note note = new Note();
        note.setName("note");
        note.setContent("text");
        note.setCreatedDate(LocalDateTime.now());

        NoteDto noteDto = new NoteDto(note, form);

        String requestContent = objectMapper.writeValueAsString(noteDto);
        this.mockMvc.perform(
                        post("/note")
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_participant", "123"))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestContent)
                )
                .andExpect(status().isForbidden());
    }
}
