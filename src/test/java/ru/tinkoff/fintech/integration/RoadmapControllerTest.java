package ru.tinkoff.fintech.integration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.jdbc.Sql;
import ru.tinkoff.fintech.form.Form;
import ru.tinkoff.fintech.form.FormRepository;
import ru.tinkoff.fintech.roadmap.Roadmap;
import ru.tinkoff.fintech.roadmap.RoadmapRepository;
import ru.tinkoff.fintech.roadmap.dto.RoadmapDto;
import ru.tinkoff.fintech.space.Space;
import ru.tinkoff.fintech.space.SpaceRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.tinkoff.fintech.form.FormType.ROADMAP;

@Sql({"/clear.sql", "/init.sql"})
public class RoadmapControllerTest extends ApplicationIntegrationTest {
    @Autowired
    private SpaceRepository spaceRepository;

    @Autowired
    private FormRepository formRepository;

    @Autowired
    private RoadmapRepository roadmapRepository;

    @Test
    public void shouldGetRoadmapNoAuth() throws Exception {
        Space space = new Space("space");
        Space savedSpace = spaceRepository.save(space);

        Form form = new Form();
        form.setTypeId(ROADMAP.formId);
        form.setHeight(100);
        form.setLength(100);
        form.setXCoordinate(0);
        form.setYCoordinate(0);
        form.setSpaceId(savedSpace.getId());
        Form savedForm = formRepository.save(form);

        Roadmap roadmap = new Roadmap();
        roadmap.setId(savedForm.getId());
        roadmap.setName("roadmap");
        roadmap.setStartDate(LocalDateTime.now());
        roadmap.setEndDate(LocalDateTime.now());

        Roadmap savedRoadmap = roadmapRepository.save(roadmap);

        this.mockMvc.perform(
                        get("/roadmap/" + savedRoadmap.getId())
                )
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void shouldGetRoadmapSuccessfully() throws Exception {
        Space space = new Space("space");
        Space savedSpace = spaceRepository.save(space);

        Form form = new Form();
        form.setTypeId(ROADMAP.formId);
        form.setHeight(100);
        form.setLength(100);
        form.setXCoordinate(0);
        form.setYCoordinate(0);
        form.setSpaceId(savedSpace.getId());
        Form savedForm = formRepository.save(form);

        Roadmap roadmap = new Roadmap();
        roadmap.setId(savedForm.getId());
        roadmap.setName("roadmap");
        roadmap.setStartDate(LocalDateTime.now());
        roadmap.setEndDate(LocalDateTime.now().plusDays(10));
        Roadmap savedRoadmap = roadmapRepository.save(roadmap);

        RoadmapDto roadmapDto = new RoadmapDto(savedRoadmap, savedForm, new ArrayList<>());

        hibernateQueryInterceptor.startQueryCount();
        String content = this.mockMvc.perform(
                        get("/roadmap/" + roadmapDto.getRoadmap().getId())
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_participant", "123"))
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        RoadmapDto resultRoadmapDto = objectMapper.readValue(content, RoadmapDto.class);
        Assertions.assertEquals(roadmapDto.getRoadmap().getName(), resultRoadmapDto.getRoadmap().getName());
        Assertions.assertEquals(5L, hibernateQueryInterceptor.getQueryCount());
    }

    @Test
    public void shouldAddRoadmapToSpaceSuccessfully() throws Exception {
        Space space = new Space("space");
        Space savedSpace = spaceRepository.save(space);

        Form form = new Form();
        form.setTypeId(ROADMAP.formId);
        form.setHeight(100);
        form.setLength(100);
        form.setXCoordinate(0);
        form.setYCoordinate(0);
        form.setSpaceId(savedSpace.getId());

        Roadmap roadmap = new Roadmap();
        roadmap.setName("roadmap");
        roadmap.setStartDate(LocalDateTime.now());
        roadmap.setEndDate(LocalDateTime.now().plusDays(10));

        RoadmapDto roadmapDto = new RoadmapDto(roadmap, form, null);

        hibernateQueryInterceptor.startQueryCount();
        String requestContent = objectMapper.writeValueAsString(roadmapDto);
        String content = this.mockMvc.perform(
                        post("/roadmap")
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_admin", "123"))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestContent)
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        Roadmap resultRoadmap = objectMapper.readValue(content, Roadmap.class);
        Assertions.assertEquals(roadmap.getName(), resultRoadmap.getName());
        Assertions.assertEquals(7L, hibernateQueryInterceptor.getQueryCount());
    }

    @Test
    public void shouldAddRoadmapToSpaceForbidden() throws Exception {
        Space space = new Space("space");
        Space savedSpace = spaceRepository.save(space);

        Form form = new Form();
        form.setTypeId(ROADMAP.formId);
        form.setHeight(100);
        form.setLength(100);
        form.setXCoordinate(0);
        form.setYCoordinate(0);
        form.setSpaceId(savedSpace.getId());

        Roadmap roadmap = new Roadmap();
        roadmap.setName("roadmap");
        roadmap.setStartDate(LocalDateTime.now());
        roadmap.setEndDate(LocalDateTime.now().plusDays(10));

        RoadmapDto roadmapDto = new RoadmapDto(roadmap, form, null);

        String requestContent = objectMapper.writeValueAsString(roadmapDto);
        this.mockMvc.perform(
                        post("/roadmap")
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_participant", "123"))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestContent)
                )
                .andExpect(status().isForbidden());
    }
}
