package ru.tinkoff.fintech.integration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.jdbc.Sql;
import ru.tinkoff.fintech.space.Space;
import ru.tinkoff.fintech.space.SpaceRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql({"/clear.sql", "/init.sql"})
public class SpaceControllerTest extends ApplicationIntegrationTest {

    @Autowired
    private SpaceRepository spaceRepository;

    @Test
    void createSpaceSuccessfully() throws Exception {
        String spaceName = "name";

        hibernateQueryInterceptor.startQueryCount();
        String content = this.mockMvc.perform(
                        post("/space?name=" + spaceName)
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_admin", "123"))
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        Space resultSpace = objectMapper.readValue(content, Space.class);

        Assertions.assertEquals(spaceName, resultSpace.getName());
        Assertions.assertEquals(4L, hibernateQueryInterceptor.getQueryCount());
    }

    @Test
    void getAllSpacesSuccessfully() throws Exception {
        String testSpaceGet = "testSpaceGet";
        String testSpaceGet2 = "testSpaceGet2";
        Space spaceOne = new Space(testSpaceGet);
        Space spaceTwo = new Space(testSpaceGet2);

        spaceRepository.saveAll(Arrays.asList(spaceOne, spaceTwo));

        hibernateQueryInterceptor.startQueryCount();
        String content = this.mockMvc.perform(
                        get("/space/all")
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_admin", "123"))
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        Assertions.assertEquals(3L, hibernateQueryInterceptor.getQueryCount());

        List<Space> spaces = Arrays.stream(objectMapper.readValue(content, Space[].class))
                .filter(el -> el.getName().equals(testSpaceGet) || el.getName().equals(testSpaceGet2))
                .toList();

        Assertions.assertEquals(2, spaces.size());
    }

    @Test
    void deleteSpaceSuccessfully() throws Exception {
        Space spaceOne = new Space("name");
        Space spaceTwo = new Space("name2");
        int expectedSize = ((List<Space>) spaceRepository.findAll()).size() + 1;

        List<Space> spaces = (List<Space>) spaceRepository.saveAll(Arrays.asList(spaceOne, spaceTwo));

        hibernateQueryInterceptor.startQueryCount();
        this.mockMvc.perform(
                        delete("/space/" + spaces.get(0).getId())
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_admin", "123"))
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        Assertions.assertEquals(4L, hibernateQueryInterceptor.getQueryCount());

        List<Space> resultSpaces = (List<Space>) spaceRepository.findAll();
        Assertions.assertEquals(expectedSize, resultSpaces.size());
    }

    @Test
    void updateSpaceSuccessfully() throws Exception {
        Space space = new Space("name");
        String newName = "new name";

        Space savedSpace = spaceRepository.save(space);
        savedSpace.setName(newName);

        hibernateQueryInterceptor.startQueryCount();
        String content = objectMapper.writeValueAsString(savedSpace);
        this.mockMvc.perform(
                        put("/space/")
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_admin", "123"))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(content)
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        Assertions.assertEquals(4L, hibernateQueryInterceptor.getQueryCount());

        Optional<Space> resultSpace = spaceRepository.findById(savedSpace.getId());
        Assertions.assertEquals(newName, resultSpace.orElse(new Space("wrong")).getName());
    }
}
