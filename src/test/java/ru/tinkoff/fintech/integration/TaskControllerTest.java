package ru.tinkoff.fintech.integration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.jdbc.Sql;
import ru.tinkoff.fintech.board.Board;
import ru.tinkoff.fintech.board.BoardRepository;
import ru.tinkoff.fintech.form.Form;
import ru.tinkoff.fintech.form.FormRepository;
import ru.tinkoff.fintech.space.Space;
import ru.tinkoff.fintech.space.SpaceRepository;
import ru.tinkoff.fintech.status.Status;
import ru.tinkoff.fintech.status.StatusBoard;
import ru.tinkoff.fintech.status.StatusBoardRepository;
import ru.tinkoff.fintech.status.StatusBoardService;
import ru.tinkoff.fintech.status.StatusService;
import ru.tinkoff.fintech.tag.Tag;
import ru.tinkoff.fintech.tag.TagRepository;
import ru.tinkoff.fintech.tag.TagService;
import ru.tinkoff.fintech.tag.task.TagTask;
import ru.tinkoff.fintech.tag.task.TagTaskRepository;
import ru.tinkoff.fintech.task.Task;
import ru.tinkoff.fintech.task.TaskRepository;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.tinkoff.fintech.form.FormType.ROADMAP;

@Sql({"/clear.sql", "/init.sql"})
public class TaskControllerTest extends ApplicationIntegrationTest {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private BoardRepository boardRepository;

    @Autowired
    private FormRepository formRepository;

    @Autowired
    private SpaceRepository spaceRepository;

    @Autowired
    private StatusBoardRepository statusBoardRepository;

    @Autowired
    private StatusBoardService statusBoardService;

    @Autowired
    private StatusService statusService;

    @Autowired
    private TagTaskRepository tagTaskRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private TagService tagService;

    @Test
    void getTaskByIdSuccessfully() throws Exception {
        Task task = prepareTask();

        hibernateQueryInterceptor.startQueryCount();
        String resultContent = this.mockMvc.perform(
                        get("/task/" + task.getId())
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_admin", "123"))
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();
        Assertions.assertEquals(3L, hibernateQueryInterceptor.getQueryCount());

        Task resultTask = objectMapper.readValue(resultContent, Task.class);
        Assertions.assertEquals(task.getName(), resultTask.getName());
    }

    @Test
    void updateTaskByIdSuccessfully() throws Exception {
        Task task = prepareTask();

        task.setName("new name");
        hibernateQueryInterceptor.startQueryCount();
        String content = objectMapper.writeValueAsString(task);
        String resultContent = this.mockMvc.perform(
                        put("/task")
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_admin", "123"))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(content)
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();
        Assertions.assertEquals(4L, hibernateQueryInterceptor.getQueryCount());

        Task resultTask = objectMapper.readValue(resultContent, Task.class);
        Assertions.assertEquals(task.getName(), resultTask.getName());
    }

    @Test
    void deleteTaskByIdSuccessfully() throws Exception {
        Task task = prepareTask();

        int expectedTaskCount = ((List<Task>) taskRepository.findAll()).size() - 1;

        hibernateQueryInterceptor.startQueryCount();
        String content = objectMapper.writeValueAsString(task);
        this.mockMvc.perform(
                        delete("/task/" + task.getId())
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_admin", "123"))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(content)
                )
                .andExpect(status().is2xxSuccessful());
        Assertions.assertEquals(5L, hibernateQueryInterceptor.getQueryCount());

        List<Task> tasks = (List<Task>) taskRepository.findAll();
        Assertions.assertEquals(expectedTaskCount, tasks.size());
    }

    @Test
    void addTagToTaskSuccessfully() throws Exception {
        Task task = prepareTask();

        String tagName = "tag";
        Tag savedTag = tagService.createTag(tagName);
        hibernateQueryInterceptor.startQueryCount();
        this.mockMvc.perform(
                        post("/task/tag/add?taskId=" + task.getId() + "&tagName=" + tagName)
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_admin", "123"))
                )
                .andExpect(status().is2xxSuccessful());
        Assertions.assertEquals(6L, hibernateQueryInterceptor.getQueryCount());

        List<TagTask> allByTagId = tagTaskRepository.findAllByTagId(savedTag.getId());
        Assertions.assertFalse(allByTagId.isEmpty());
    }

    @Test
    void getTasksByTagNameSuccessfully() throws Exception {
        Task task = prepareTask();

        String tagName = "tag";
        Tag tag = new Tag(tagName);
        Tag savedTag = tagRepository.save(tag);
        tagTaskRepository.save(new TagTask(task.getId(), savedTag.getId()));

        hibernateQueryInterceptor.startQueryCount();
        String resultContent = this.mockMvc.perform(
                        get("/task/tag/" + tagName)
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_admin", "123"))
                )
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();
        Assertions.assertEquals(4L, hibernateQueryInterceptor.getQueryCount());

        List<Long> tasksId = Arrays.asList(objectMapper.readValue(resultContent, Long[].class));

        Assertions.assertEquals(1, tasksId.size());
        Assertions.assertEquals(task.getId(), tasksId.get(0));
    }

    @Test
    void deleteTagFromTaskSuccessfully() throws Exception {
        Task task = prepareTask();

        String tagName = "tag";
        Tag savedTag = tagService.createTag(tagName);
        int expectedSize = tagTaskRepository.findAllByTagId(savedTag.getId()).size();
        tagTaskRepository.save(new TagTask(task.getId(), savedTag.getId()));

        hibernateQueryInterceptor.startQueryCount();
        this.mockMvc.perform(
                        delete("/task/" + task.getId() + "/delete/tag/" + savedTag.getId())
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_admin", "123"))
                )
                .andExpect(status().is2xxSuccessful());
        Assertions.assertEquals(4L, hibernateQueryInterceptor.getQueryCount());

        List<TagTask> allByTagId = tagTaskRepository.findAllByTagId(savedTag.getId());
        Assertions.assertEquals(expectedSize, allByTagId.size());
    }

    private Task prepareTask() {
        Space space = new Space();
        space.setName("name");

        Space savedSpace = spaceRepository.save(space);

        Form form = new Form();
        form.setSpaceId(savedSpace.getId());
        form.setTypeId(ROADMAP.formId);

        Form savedForm = formRepository.save(form);

        Board board = new Board();
        board.setId(savedForm.getId());
        board.setName("name");
        board.setDescription("description");

        Board savedBoard = boardRepository.save(board);

        Status orGetStatus = statusService.createOrGetStatus("In progress");
        statusBoardService.createStatusBoard(new StatusBoard(board.getId(), orGetStatus.getId()));

        Task task = new Task();
        task.setName("task");
        task.setDescription("description");
        task.setBoardId(savedBoard.getId());
        task.setStatusId(orGetStatus.getId());

        return taskRepository.save(task);
    }
}
