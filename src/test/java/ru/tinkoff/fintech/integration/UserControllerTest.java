package ru.tinkoff.fintech.integration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.jdbc.Sql;
import ru.tinkoff.fintech.role.Role;
import ru.tinkoff.fintech.role.RoleService;
import ru.tinkoff.fintech.user.User;
import ru.tinkoff.fintech.user.UserRepository;
import ru.tinkoff.fintech.user.dto.UserDto;

import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.tinkoff.fintech.security.SecurityConfiguration.PARTICIPANT_ROLE_NAME;

@Sql({"/clear.sql", "/init.sql"})
public class UserControllerTest extends ApplicationIntegrationTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleService roleService;

    @Test
    void registrateNewUserSuccessfully() throws Exception {
        UserDto userDto = new UserDto();
        userDto.setName("name");
        userDto.setUsername("name@mail.ru");
        userDto.setPassword("password123PWD!@#");

        String content = objectMapper.writeValueAsString(userDto);

        hibernateQueryInterceptor.startQueryCount();
        this.mockMvc.perform(
                        post("/user/registration")
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_admin", "123"))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(content)
                )
                .andExpect(status().is2xxSuccessful());
        Assertions.assertEquals(4L, hibernateQueryInterceptor.getQueryCount());

        Optional<User> maybeUser = userRepository.findById(userDto.getUsername());
        Assertions.assertTrue(maybeUser.isPresent());

        Role roleById = roleService.getRoleById(maybeUser.get().getRoleId());
        Assertions.assertEquals(userDto.getName(), maybeUser.get().getFullName());
        Assertions.assertEquals(roleById.getName(), PARTICIPANT_ROLE_NAME);
    }

    @Test
    void registrateNewUserWithWrongPassword() throws Exception {
        UserDto userDto = new UserDto();
        userDto.setName("name");
        userDto.setUsername("name@mail.ru");
        userDto.setPassword("password1");

        String content = objectMapper.writeValueAsString(userDto);

        this.mockMvc.perform(
                        post("/user/registration")
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_admin", "123"))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(content)
                )
                .andExpect(status().isBadRequest());
    }


    @Test
    void registrateNewUserWithWrongUsername() throws Exception {
        UserDto userDto = new UserDto();
        userDto.setName("name");
        userDto.setUsername("name");
        userDto.setPassword("password1");

        String content = objectMapper.writeValueAsString(userDto);

        this.mockMvc.perform(
                        post("/user/registration")
                                .with(SecurityMockMvcRequestPostProcessors.httpBasic("test_admin", "123"))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(content)
                )
                .andExpect(status().isBadRequest());
    }
}
