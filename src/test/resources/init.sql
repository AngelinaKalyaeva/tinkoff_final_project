insert into users values ('test_admin', 'admin', '$2a$10$zDZ2e5VXhaTka/DTEXdhZO0ZrMuupvrdc2ayHbmw98wD0SyAU0AVe', 1);
insert into users values ('test_participant', 'participant', '$2a$10$zDZ2e5VXhaTka/DTEXdhZO0ZrMuupvrdc2ayHbmw98wD0SyAU0AVe', 2);
